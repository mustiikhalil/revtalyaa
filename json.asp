[{
	"ID": 7759,
	"NAME": "Nuh'un Gemisi Deluxe Hotel & SPA",
	"SUBDOMAIN": "nuhungemisi",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1370870587433.png"
}, {
	"ID": 7773,
	"NAME": "Seyhan Otel Adana",
	"SUBDOMAIN": "seyhan",
	"CITY": "Adana, Turkey"
}, {
	"ID": 7789,
	"NAME": "Club Belcekız",
	"SUBDOMAIN": "belcekiz",
	"CITY": "Mugla, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1371214353562.png"
}, {
	"ID": 7790,
	"NAME": "Bey-Evi Alacatı",
	"SUBDOMAIN": "beyevi",
	"CITY": "Izmir, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/MyHotelFiles/1373895896_s.jpg"
}, {
	"ID": 7797,
	"NAME": "Fresco Cave Suites",
	"SUBDOMAIN": "fresco",
	"CITY": "Nevsehir, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1376992620341.jpg"
}, {
	"ID": 7823,
	"NAME": "Hotel Villa Granada",
	"SUBDOMAIN": "villagranada",
	"CITY": "Antalya, Turkey",
	"IMAGESMALLURL": "https://webhotel.s3.amazonaws.com/s1404631102.JPG"
}, {
	"ID": 7826,
	"NAME": "Ommer Hotel",
	"SUBDOMAIN": "ommer",
	"CITY": "Kayseri, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1419262595012-400x400.jpeg"
}, {
	"ID": 7832,
	"NAME": "Ada Palas Butik Otel",
	"SUBDOMAIN": "adapalas",
	"CITY": "Istanbul, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1372164472624.png"
}, {
	"ID": 7868,
	"NAME": "LykiaWorld & LinksGolf Antalya",
	"SUBDOMAIN": "lykiaworld",
	"CITY": "Antalya, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1414138631324-400x400.jpeg"
}, {
	"ID": 8029,
	"NAME": "Euro Park Otel",
	"SUBDOMAIN": "europark",
	"CITY": "Istanbul, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1417512882769-400x400.jpeg"
}, {
	"ID": 8076,
	"NAME": "Alesta Yacht Hotel",
	"SUBDOMAIN": "alestayacht",
	"CITY": "Fethiye, Turkey",
	"IMAGESMALLURL": "https://webhotel.s3.amazonaws.com/s1382619304.jpg"
}, {
	"ID": 8115,
	"NAME": "La Boutique Antalya - Adult Only",
	"SUBDOMAIN": "laboutique",
	"CITY": "Antalya, Turkey",
	"IMAGESMALLURL": "https://webhotel.s3.amazonaws.com/1407929123.jpg"
}, {
	"ID": 8126,
	"NAME": "Alaiye Resort & Spa Hotel",
	"SUBDOMAIN": "alaiyeresort",
	"CITY": "Antalya, Turkey",
	"IMAGESMALLURL": "https://webhotel.s3.amazonaws.com/s1384424938.jpg"
}, {
	"ID": 8127,
	"NAME": "Alaiye Kleopatra Hotel",
	"SUBDOMAIN": "alaiyekleopatra",
	"CITY": "Antalya, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/MyHotelFiles/1 MAIN BUILDING_s.jpg"
}, {
	"ID": 8137,
	"NAME": "The Bay Beach Club",
	"SUBDOMAIN": "thebaybeachclub",
	"CITY": "Mugla, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1463477120898-64x64-400x400-800x800.jpeg"
}, {
	"ID": 8143,
	"NAME": "Balsamo Suit",
	"SUBDOMAIN": "balsamo",
	"CITY": "Istanbul, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/8143/e6ea6711-05f7-410b-bf40-81bffcfafa16_s.jpg"
}, {
	"ID": 8191,
	"NAME": "Melekler Evi",
	"SUBDOMAIN": "meleklerevi",
	"CITY": "Nevsehir, Turkey",
	"IMAGESMALLURL": "https://webhotel.s3.amazonaws.com/s1389692901.png"
}, {
	"ID": 8289,
	"NAME": "GoldCity Hotel",
	"SUBDOMAIN": "goldcity",
	"CITY": "Antalya, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/156/MyHotelFiles/1477568788924-64x64-400x400-800x800.jpeg"
}, {
	"ID": 8290,
	"NAME": "Sentido Gold Island Hotel",
	"SUBDOMAIN": "goldisland",
	"CITY": "Antalya, Turkey",
	"IMAGESMALLURL": "https://webhotel.s3.amazonaws.com/s1390833109.JPG"
}, {
	"ID": 8292,
	"NAME": "Ridos Termal Otel",
	"SUBDOMAIN": "ridos",
	"CITY": "Rize, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1476268562909-64x64-400x400-800x800.jpeg"
}, {
	"ID": 8350,
	"NAME": "Highlight Hotel",
	"SUBDOMAIN": "highlight",
	"CITY": "Mugla, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1424252518057-400x400.jpeg"
}, {
	"ID": 8398,
	"NAME": "Ramada Plaza İzmir",
	"SUBDOMAIN": "ramadaplazaizmir",
	"CITY": "Izmir, Turkey",
	"IMAGESMALLURL": "https://webhotel.s3.amazonaws.com/s1406106404.jpg"
}, {
	"ID": 8414,
	"NAME": "Baga Otel Gökova",
	"SUBDOMAIN": "baga",
	"CITY": "Mugla, Turkey",
	"IMAGESMALLURL": "https://webhotel.s3.amazonaws.com/s1394871551.jpg"
}, {
	"ID": 8428,
	"NAME": "Tuntas Beach Hotel Altinkum",
	"SUBDOMAIN": "tuntasbeach",
	"CITY": "Didim, Turkey",
	"IMAGESMALLURL": "https://webhotel.s3.amazonaws.com/s1403083205.JPG"
}, {
	"ID": 8430,
	"NAME": "Tuntas Suites Altinkum",
	"SUBDOMAIN": "tuntassuites",
	"CITY": "Didim, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/MyHotelFiles/apartd-pool-2_s.jpg"
}, {
	"ID": 8432,
	"NAME": "Tuntas Family Suites Kusadasi",
	"SUBDOMAIN": "tuntasfamily",
	"CITY": "Aydin, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/MyHotelFiles/7O9A7390_s.jpg"
}, {
	"ID": 8462,
	"NAME": "Kazdağları Hattusa Astyra Thermal Resort & Spa",
	"SUBDOMAIN": "hattusa",
	"CITY": "Edremit (Balikesir), Turkey",
	"IMAGESMALLURL": "https://webhotel.s3.amazonaws.com/s1396359112.jpg"
}, {
	"ID": 8469,
	"NAME": "Han Boutique Hotel",
	"SUBDOMAIN": "hanboutique",
	"CITY": "Mugla, Turkey",
	"IMAGESMALLURL": "https://webhotel.s3.amazonaws.com/s1396513716.jpg"
}, {
	"ID": 8521,
	"NAME": "The Merlot Hotel Eskişehir",
	"SUBDOMAIN": "merlot",
	"CITY": "Eskisehir, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1437215350828-400x400.jpeg"
}, {
	"ID": 8541,
	"NAME": "Rezone Health & Oxygen Hotel",
	"SUBDOMAIN": "rezonehotel",
	"CITY": "Edremit (Balikesir), Turkey",
	"IMAGESMALLURL": "https://webhotel.s3.amazonaws.com/s1400481950.jpg"
}, {
	"ID": 8759,
	"NAME": "Cuci Hotel di Mare - Bayramoglu",
	"SUBDOMAIN": "cuci",
	"CITY": "Izmit, Turkey"
}, {
	"ID": 8787,
	"NAME": "Bera Alanya",
	"SUBDOMAIN": "beraalanya",
	"CITY": "Antalya, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/156/MyHotelFiles/1477567584618-64x64-400x400-800x800.jpeg"
}, {
	"ID": 8806,
	"NAME": "Bera Konya",
	"SUBDOMAIN": "berakonya",
	"CITY": "Konya, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1411388460511-400x400.jpeg"
}, {
	"ID": 8808,
	"NAME": "Bera Mevlana",
	"SUBDOMAIN": "h55214s",
	"CITY": "Konya, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1451723991102-400x400.jpeg"
}, {
	"ID": 8841,
	"NAME": "Star Royal",
	"SUBDOMAIN": "starroyal",
	"CITY": "Igdir, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1415710984856-400x400.jpeg"
}, {
	"ID": 8916,
	"NAME": "Abacı Otel",
	"SUBDOMAIN": "abaci",
	"CITY": "Eskisehir, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1424098539391-400x400.jpeg"
}, {
	"ID": 9104,
	"NAME": "Dosi Hotel",
	"SUBDOMAIN": "dosi",
	"CITY": "Side, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1422875039127-400x400.jpeg"
}, {
	"ID": 9341,
	"NAME": "Upper House Hotel",
	"SUBDOMAIN": "upperhouse",
	"CITY": "Antalya, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/9341/0ecfb3e4-a82f-42e5-b97d-ad7264f18a58_s.jpg"
}, {
	"ID": 9490,
	"NAME": "Kalyon Çeşme ",
	"SUBDOMAIN": "kalyon",
	"CITY": "Cesme, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/9490/530b95ab-97e5-4581-9ab5-b37271969797_s.jpg"
}, {
	"ID": 9496,
	"NAME": "Casa dell'Arte Bodrum",
	"SUBDOMAIN": "casadellart",
	"CITY": "Mugla, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1428048822334-400x400.jpeg"
}, {
	"ID": 9509,
	"NAME": "Wome Hotel",
	"SUBDOMAIN": "wome",
	"CITY": "Antalya, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/MyHotelFiles//acc9ee14-258e-45a8-b7de-fdd3b3a19523_8148P_s.jpg"
}, {
	"ID": 9513,
	"NAME": "Perdue Hotel",
	"SUBDOMAIN": "perdue",
	"CITY": "Mugla, Turkey"
}, {
	"ID": 9514,
	"NAME": "Nautical Faralya",
	"SUBDOMAIN": "nautical",
	"CITY": "Mugla, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1429608204633-400x400.jpeg"
}, {
	"ID": 9524,
	"NAME": "Turunç Eskişehir Hotel",
	"SUBDOMAIN": "turunceskxx",
	"CITY": "Eskisehir, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1430141711587-400x400.jpeg"
}, {
	"ID": 9668,
	"NAME": "Köşk Orman - Büyükada",
	"SUBDOMAIN": "koskorman",
	"CITY": "Istanbul, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1433158865487-400x400.jpeg"
}, {
	"ID": 9723,
	"NAME": "Jiva Beach Resort",
	"SUBDOMAIN": "jiva",
	"CITY": "Mugla, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1435146539627-400x400.jpeg"
}, {
	"ID": 9820,
	"NAME": "Büyükada Apart Hotel - Sehbal",
	"SUBDOMAIN": "buyukadaapart1",
	"CITY": "Istanbul, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1438956121041-400x400.jpeg"
}, {
	"ID": 9821,
	"NAME": "Büyükada apart Hotel - Karadağ",
	"SUBDOMAIN": "buyukadaapart2",
	"CITY": "Istanbul, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1438956195871-400x400.jpeg"
}, {
	"ID": 9822,
	"NAME": "Büyükada Apart Hotel - Yeni  Apart",
	"SUBDOMAIN": "buyukadaapart3",
	"CITY": "Istanbul, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1438956229837-400x400.jpeg"
}, {
	"ID": 10004,
	"NAME": "Élite Hotel Darıca",
	"SUBDOMAIN": "elitedarica",
	"CITY": "Izmit, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/MyHotelFiles/2404457_s.jpg"
}, {
	"ID": 10229,
	"NAME": "Sentido Perissia",
	"SUBDOMAIN": "sentido",
	"CITY": "Antalya, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1443087526499.jpg"
}, {
	"ID": 10234,
	"NAME": "Kaya Magnesia Spa & Wellness",
	"SUBDOMAIN": "kayamagnesia",
	"CITY": "Manisa, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1443686327801-400x400.jpeg"
}, {
	"ID": 10257,
	"NAME": "Svalinn Hotel",
	"SUBDOMAIN": "svalinn",
	"CITY": "Izmir, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1442224645033-400x400.jpeg"
}, {
	"ID": 10541,
	"NAME": "Modernity Hotel",
	"SUBDOMAIN": "modernity",
	"CITY": "Eskisehir, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1448026988155-400x400.jpeg"
}, {
	"ID": 10576,
	"NAME": "Nizam Butik Hotel",
	"SUBDOMAIN": "nizambutik",
	"CITY": "Istanbul, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/MyHotelFiles//IMG_6175 copy_s.jpg"
}, {
	"ID": 10673,
	"NAME": "RYS Hotel",
	"SUBDOMAIN": "rysotel",
	"CITY": "Edirne, Turkey"
}, {
	"ID": 10756,
	"NAME": "Kimera Hotel",
	"SUBDOMAIN": "kimerax",
	"CITY": "Antalya, Turkey",
	"IMAGESMALLURL": "https://s3-eu-west-1.amazonaws.com/webhotel/images/1452865595543-400x400.jpeg"
}, {
	"ID": 10757,
	"NAME": "Mimoza Pansiyon1",
	"SUBDOMAIN": "mimoza1",
	"CITY": "Istanbul, Turkey"
}, {
	"ID": 10773,
	"NAME": "Mirage Park Resort",
	"SUBDOMAIN": "mirageparkresort",
	"CITY": "Antalya, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/MyHotelFiles/ana-bina-standart1_s.jpg"
}, {
	"ID": 10827,
	"NAME": "Club Hotel Rama ",
	"SUBDOMAIN": "rama",
	"CITY": "Antalya, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/10827/681d9a6a-d69e-4905-bbd3-f588087e6425_s.jpg"
}, {
	"ID": 10859,
	"NAME": "Nearport Hotel",
	"SUBDOMAIN": "nearport",
	"CITY": "Istanbul, Turkey"
}, {
	"ID": 11251,
	"NAME": "Adin Beach Hotel",
	"SUBDOMAIN": "adinbeach",
	"CITY": "Antalya, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/11251/d2938b86-2b21-4486-aa89-bec1bbb493ac_s.jpg"
}, {
	"ID": 11491,
	"NAME": "Armonia Holiday Village & Spa",
	"SUBDOMAIN": "armonia",
	"CITY": "Mugla, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/11491/c69a33a7-2cb6-47c5-9da2-37859fb9aadf_s.jpg"
}, {
	"ID": 12220,
	"NAME": "Mardinn Hotel",
	"SUBDOMAIN": "mardinn",
	"CITY": "Istanbul, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/MyHotelFiles/6uyjgh_s.jpg"
}, {
	"ID": 12227,
	"NAME": "Grand Sunlife Otel",
	"SUBDOMAIN": "grand-sunlife",
	"CITY": "Alanya, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/MyHotelFiles/oda1_s.jpg"
}, {
	"ID": 12232,
	"NAME": "Happy Hotel",
	"SUBDOMAIN": "happyhotel",
	"CITY": "Kalkan, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/MyHotelFiles/happy-hotel-kalkan_s.jpg"
}, {
	"ID": 12341,
	"NAME": "Boutique Hotel Baku",
	"SUBDOMAIN": "boutique-hotel",
	"CITY": "Baku, Azerbaijan",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/MyHotelFiles/1K4A1645-1024x683_s.jpg"
}, {
	"ID": 12405,
	"NAME": "Mia City Hotel",
	"SUBDOMAIN": "mia-city",
	"CITY": "Izmir, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/MyHotelFiles/_44A4634_s.jpg"
}, {
	"ID": 12504,
	"NAME": "Tasigo Hotels Eskişehir Bademlik Termal",
	"SUBDOMAIN": "tasigo-hotels",
	"CITY": "Eskisehir, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/MyHotelFiles/tasigo-otel1_s.jpg"
}, {
	"ID": 12570,
	"NAME": "Aquasis Deluxe Resort & Spa",
	"SUBDOMAIN": "aquasisdeluxe",
	"CITY": "Aydin, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/MyHotelFiles/GENERAL VIEW_s.jpg"
}, {
	"ID": 12659,
	"NAME": "Al Bahir Deluxe Hotel",
	"SUBDOMAIN": "albahir",
	"CITY": "Antalya, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/MyHotelFiles/albahir-genel-goruntu_s.jpg"
}, {
	"ID": 13125,
	"NAME": "Club Hotel Anjeliq  ",
	"SUBDOMAIN": "anjeliq",
	"CITY": "Antalya, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/MyHotelFiles/1_s.jpg"
}, {
	"ID": 13231,
	"NAME": "SeydaOtel",
	"SUBDOMAIN": "seydaotel",
	"CITY": "Antalya, Turkey"
},  {
	"ID": 13294,
	"NAME": "Opera Hotel",
	"SUBDOMAIN": "opera",
	"CITY": "Baku, Azerbaijan",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/MyHotelFiles//opera_hotel2_s.jpg"
}, {
	"ID": 13539,
	"NAME": "Burgaz İzer Hotel",
	"SUBDOMAIN": "burgazizer",
	"CITY": "Kirklareli, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/MyHotelFiles//b687dc72-e6bd-4543-bc82-c46ecb6bb7ba_sdfsdgsdfbxfvbd_s.jpg"
}, {
	"ID": 13622,
	"NAME": "B&B Yüzbaşı Hotel",
	"SUBDOMAIN": "bb-yuzbasi",
	"CITY": "Marmaris, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/MyHotelFiles//b3be4b8a-d9ba-4e97-9afb-d2cf5432462b_250A0719JPG_s.jpg"
}, {
	"ID": 13640,
	"NAME": "Villa Mahal",
	"SUBDOMAIN": "villamahal",
	"CITY": "Antalya, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/MyHotelFiles//cliff house (4)-min_s.jpg"
}, {
	"ID": 13755,
	"NAME": "Liparis Resort Hotel & Spa",
	"SUBDOMAIN": "liparis",
	"CITY": "Mersin, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/MyHotelFiles//1_s.jpg"
}, {
	"ID": 13787,
	"NAME": "Liberty Hotels Lykia",
	"SUBDOMAIN": "liberty",
	"CITY": "Mugla, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/MyHotelFiles//_DSC8637_s.jpg"
}, {
	"ID": 13945,
	"NAME": "Palm Wings Beach Resort Didim",
	"SUBDOMAIN": "palmwings",
	"CITY": "Aydin, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/239/MyHotelFiles/559e45ad-aa17-4317-b788-e78a394c212a_palm_s.jpg"
}, {
	"ID": 14031,
	"NAME": "Sarot Termal Park Resort & Spa",
	"SUBDOMAIN": "sarot-termal",
	"CITY": "Bolu, Turkey",
	"IMAGESMALLURL": "https://portalv3.s3.eu-central-1.amazonaws.com/1/MyHotelFiles//6c4f9b92-c2f4-4ed1-999e-db2897b42530_oda_1_5_s.jpg"
}]