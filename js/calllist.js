
var hotelId = null

var HotelIDFromURL =function () {
    var getID = window.location.href.split("id=").slice(-1)[0]
    getID = getID.split("&");
    var pasrseID = getID[0].slice(2)
    hotelId = pasrseID.slice(0,-2)
}

var ref = null

function getHotelListOfReservations(){

    var password = document.getElementById('password').value
    ref = document.getElementById('refID').value

    if (ref !== "" && ref !== null){
        $.ajax({
            type: "Get",
            url: 'https://api.hoteladvisor.net/Hotel/BuyLaterList?hotelid='+hotelId+'&password='+ password + '&refid=' + ref,
            success: function (respones) {
                document.getElementById('entryPoint').setAttribute('style','display: none')
                document.getElementById('tableDiv').removeAttribute('style')
                displayInfo(respones)

            }
        });
    }
    else{
        $.ajax({
            type: "Get",
            url: 'https://api.hoteladvisor.net/Hotel/BuyLaterList?hotelid='+hotelId+'&password='+ password,
            success: function (respones) {
                document.getElementById('entryPoint').setAttribute('style','display: none')
                document.getElementById('tableDiv').removeAttribute('style')
                displayInfo(respones)

            }
        });
    }
}


function displayInfo(respones){
    for (var i = 0; i < respones.length; i++){

        var date = new moment(respones[i].RequestDate)
        var buyLater = respones[i].RequestId
        document.getElementById('tableList').innerHTML += `<tr><td><div><ul>
                                                            <li>`+date.format('lll') +`</li>
                                                            <li>`+respones[i].GuestFullName+`</li>
                                                            <li>`+respones[i].GuestPhone+`  <a href="tel:`+respones[i].GuestPhone+`"><i class=" fa fa-phone" aria-hidden="true"></i></a></li>
                                                            <li>`+respones[i].GuestEmail+`  <a href='mailto:`+respones[i].GuestEmail+`' target="_top"><i class=" fa fa-envelope" aria-hidden="true"></i></a></li></ul>`+respones[i].Message+`</div>
                                                          
                                                            <div class="col-xs-12">
                                                            <button class="col-xs-4 btn btn-danger" onclick="removeInfo('`+buyLater+`')">cancel</button>
                                                            </div></tr></td>`
    }
}

function goBackToreservationPage(){
    const goback = window.location.href.split("calllist.html");
    var getId = goback[1].split('id=')
    window.location.href = goback[0]+"?id=" + getId[1]

}



function GetDates(dateGiven){
    var requestMonth = ((moment(dateGiven, 'DD.MM.YYYY').get('month')) +1 > 9) ? (moment(dateGiven, 'DD.MM.YYYY').get('month')+1 ).toString() : '0'+ (moment(dateGiven, 'DD.MM.YYYY').get('month') +1)

    var requestDay = (moment(dateGiven, 'DD.MM.YYYY').get('date') > 9) ? moment(dateGiven, 'DD.MM.YYYY').get('date').toString() : '0'+ moment(dateGiven, 'DD.MM.YYYY').get('date')

    requestDate =   requestDay+ '.'+requestMonth + '.' + moment(dateGiven, 'DD.MM.YYYY').get('year').toString()
    return requestDate
}


function removeInfo(x){
    $.ajax({
        type: "Get",
        url: 'https://api.hoteladvisor.net/Hotel/BuyLaterDelete/?buylater='+x,
        success: function (respones) {
            if (respones){
                var table = document.getElementById('tableList')

                while (table.hasChildNodes()) {
                    table.removeChild(table.lastChild);
                }
                getHotelListOfReservations()
            }
        }
    });
}












