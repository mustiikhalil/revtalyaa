﻿/**
 *  Gets the hotel config
 * @param id is the hotel id number ?id=121220612
 */
function getHotelconfigAJAXID(id){
    $.ajax({
        type: "GET",
        url: "https://api.hoteladvisor.net/Hotel/getHotelConfig?id=" + id,

        success: function (respond) {

            if (respond != null) {
                setParameters(respond)
            }
            else {
            }

        }
    });
}

/**
 *  Gets the hotel config
 * @param id is the hotel subdomain #hotel123
 */

function getHotelConfigAJAXSubdomain(x){

    $.ajax({
        type: "GET",
        url: "https://api.hoteladvisor.net/Hotel/GetHotelConfig?subdomain=" + x,

        success: function (respond) {

            if (respond == null) {
                alert("Hotel link is not correct")
            }
            else {
                hotelId = respond.HotelID
                setParameters(respond)

            }


        }
    });
}

/**
 *  Gets the room config
 * @param thisURL holds the url with a query that has a checkin, checkout, adults and children attached to it.
 * @gets an array full of rooms
 */
function getRoomsAJAXCall(thisURL){
    $.ajax({
        type: "GET",
        url: thisURL,

        success: function (respones) {

            rooms = respones

            fillInRooms(respones);
            $(".roomPanel").css("display","block");
            $('html,body').animate({ scrollTop: $("#rooms").offset().top-120}, 500);
            $('#searchHeader').removeClass('active');
            $('#roomHeader').addClass('active');
        }
    });
}

/**
 *  Sends the postSend object that is created in custom.js to the server and it holds all the information needed
 *  to purchase the room
 * @param postSend
 */
function postRequestAJAX(postSend){

    var resID = null
    var surname  = document.getElementById('surname1').value
    $.ajax({
        type: "POST",
        url: "https://api.hoteladvisor.net/Hotel/SaveReservation",
        data: postSend,
        success: function(respones){

            if (respones.ReservationId === -1 && respones.PaymentRequired){

                document.location = respones.PaymentUrl

            }
            else if (respones.ReservationId === -1){
                $( function () {
                  alert('Bu Rezervasyon ( ID: '+ respones.Message.slice(42,48)+' ) daha önceden alınmıştır.')
                })}
            else{
                resID = respones.ReservationId
                if (resID !== null && resID !== -1){
                    $.ajax({
                        type: "GET",
                        url: 'https://api.hoteladvisor.net/Hotel/CheckReservation?resid='+resID+'&surname=' + surname,
                        success: function (respones) {
                            document.getElementById('fullHTML').classList.add('blur')
                            document.getElementById('floatingPayment').setAttribute('style','display: block !important')
                            fillInReservationCompletedOrShow(respones)
                        }
                    });
                }
            }

        }
    });
}

/**
 *  Sends the postSend object that is created in custom.js to the server and it holds all the information needed
 *  to save the room and buy later
 * @param x
 */
function saveResAJAXCall(x){
    console.log(x)
    $.ajax({
        type: "POST",
        url: "https://api.hoteladvisor.net/Hotel/BuyLaterSet",
        data: x,
        success: function (respones) {
            if  (respones.Success) {
                alert("Rezervasyonunuz Kayıt Edilmiştir. Kayıt Numaranız: " + respones.RequestId)
            }
            else{
                alert("Error: "+ respones.Message)
            }
        }
    });
}

/**
 *  get's the buylater which would have the id of the buy later and fills in the information
 * @param x
 */
function showSavedReservation(x){
    $.ajax({
        type: "GET",
        url: "https://api.hoteladvisor.net/Hotel/BuyLaterGet/?buylater="+x,
        data: postSend,
        success: function (respones) {
            if (respones.Success){
                waitingForHotelParametersToBeSet(respones)
            }
            else{
                alert(respones.Message)
            }

        }
    });
}

/**
 *  Gets the rooms for the saved reservation so we can find the room that the customer has picked.
 * @param URL
 * @param parsedJsonSavedRes is just passed through to showRoomThatWasSaved()
 */
function findSavedRoom(URL,parsedJsonSavedRes){
    $.ajax({
        type: "GET",
        url: URL,
        success: function (respones) {

            savedResRooms = respones
            showRoomThatWasSaved(savedResRooms,parsedJsonSavedRes)
        }
    });
}

/**
 *  Gets the hotel config for the saved reservation.
 * @param x is the hotel id
 * @param parsedJsonSavedRes is just passed through to showRoomThatWasSaved()
 */
function getHotelconfigAJAXIDForSavedRes(x,parsedJsonSavedRes){
    $.ajax({
        type: "GET",
        url: "https://api.hoteladvisor.net/Hotel/getHotelConfig?id=" + x,

        success: function (respond) {

            if (respond != null) {
                setParametersForRese(respond)
                setParam(parsedJsonSavedRes)
            }
            else {
            }


        }
    });
}