﻿/**
 * list of global variables that would be used in the code
*/

let screenWidth = $(window).width();
var radioValue = null
var selectedLang = null;

var mapLink

var languageDictonary;

var datePickerCheckInDate;
var datePickerCheckOutDate;

var resposeForShowReservation;
// will be set to the hotel ID
var hotelId = null;

// Global var to get the first request json
var searchConfig;

// Global var to get the rooms from the request json
var rooms = null;

var reasonToCheckIn = "Minimum date you can pick to Check In"
var reasonToCheckOut = "Minimum date you can pick to Check out"

var pickeddatePickerCheckInDate;
var pickeddatePickerCheckOutDate;

var maxPeople = {
	maxPax: 15,
	maxAdult: 10,
	maxChild: 5,
};

// the amount of people selected
var peopleSelected = {
    adult: 0,
    child: 0
};



// this is the JSON that will be sent to the server
var postSend = {
    HotelID: "",
    Arrival: "",
    Departure: "",
    Adult: 1,
    ChdAges: "",
    Currency: "",
    RoomTypeId: 0,
    RateTypeId: 0,
    BoardTypeId: 0,
    Price: 0.0,
    Email: "",
    Phone: "",
    PaymentType: 0,
    CardHolder: "",
    CardNo: "",
    CardExpires: "",
    CardCvv: "",
    People: [],
    BuyLaterId: "",
    PortalReferenceId: "",
    Extras: [],
}

var roomHeaderString;

//Shows the difference between checkIn and checkOut dates
var differenceBetweenCheckInAndOut;

var roomIsChoosen = null


/**
 *  this object is going to be sent over Post request later.
 *
 * @param gender the gender of the person
 * @param date birthdate of the person
 * @param name name of the person
 * @param surname surname of the person
 */
function person(gender,date,name,surname) {
    this.Gender = gender
    this.BirthDate = date
    this.FirstName = name
    this.LastName = surname
}
/**
 * End of the list
 */

/**
 * This is a cookie Reader that would get the language from the cookies or will check the default browser settings
 */

if (readCookie('languageCookie') !== null && readCookie('languageCookie') !== undefined) {
    selectedLang = readCookie('languageCookie').toUpperCase()

}
else{
    var lan = navigator.language.split("-")
    selectedLang = lan[0].toLowerCase()
}

/**
 * this Function get's the dates from the datetimepickers and formats them in 1994-08-19
 *
 * returns: Array that has the checkIn and CheckOut dates
 * */

function GetDates(){
    var monthCheckIn = ((moment(document.getElementById('datetimepicker1').value, 'DD.MM.YYYY').get('month')) +1 > 9) ? (moment(document.getElementById('datetimepicker1').value, 'DD.MM.YYYY').get('month')+1 ).toString() : '0'+ (moment(document.getElementById('datetimepicker1').value, 'DD.MM.YYYY').get('month') +1)
    var monthCheckOut = ((moment(document.getElementById('datetimepicker2').value, 'DD.MM.YYYY').get('month')) +1 > 9) ? (moment(document.getElementById('datetimepicker2').value, 'DD.MM.YYYY').get('month')+1 ).toString() : '0'+ (moment(document.getElementById('datetimepicker2').value, 'DD.MM.YYYY').get('month') + 1)

    var dayCheckIn = (moment(document.getElementById('datetimepicker1').value, 'DD.MM.YYYY').get('date') > 9) ? moment(document.getElementById('datetimepicker1').value, 'DD.MM.YYYY').get('date').toString() : '0'+ moment(document.getElementById('datetimepicker1').value, 'DD.MM.YYYY').get('date')
    var dayChecOut = (moment(document.getElementById('datetimepicker2').value, 'DD.MM.YYYY').get('date') > 9) ? moment(document.getElementById('datetimepicker2').value, 'DD.MM.YYYY').get('date').toString() : '0'+ moment(document.getElementById('datetimepicker2').value, 'DD.MM.YYYY').get('date')
    CheckInDate =  moment(document.getElementById('datetimepicker1').value, 'DD.MM.YYYY').get('year').toString() + '-'+ monthCheckIn+ '-' + dayCheckIn
    CheckOutDate =  moment(document.getElementById('datetimepicker2').value, 'DD.MM.YYYY').get('year').toString() + '-'+monthCheckOut + '-' + dayChecOut
    return [CheckInDate,CheckOutDate]
}


/**
 * This is called to check the URL and what information it holds.
 *
 * the language dictonary will be set here to the langauge that is taken from the cookies or the default browser settings
 *
 *
 * resID: will check if the URL has a resID which means that the booking is completed
 * buyLater: will check if the URL has an ID for the saved reservation
 * ID: will get the hotel ID from the URL
 * Else the hotel ID would be give in the form of .xyz/#demohotel
 * */

var parseHotelIDFromURL =function () {

    // returns the dictonary that is picked after changing some of the labels that were set.
    languageDictonary = setLanguageAndReturnDictonary(selectedLang)

	var URLText = window.location.href;

    if (URLText.includes('resid') === true){
        document.getElementById('fullHTML').classList.add('blur')
        document.getElementById('floatingPayment').setAttribute('style','display: block !important')
        document.getElementById('findMyReservation').style = 'display: none'
        document.getElementById('paymentDone').removeAttribute('style')
        document.getElementById('acceptPayment').innerHTML  = languageDictonary.ConfID +': '
        document.getElementById('acceptPaymentMessage').innerHTML = URLText.split("&").filter(function (f) { return f.indexOf("msg")==0})[0].split("=")[1];
        document.getElementById('acceptPaymentRequest').innerHTML  = URLText.split("&").filter(function(f){ return f.indexOf("resid") == 0 })[0].split("=")[1];
    }
    else if (URLText.includes('buylater') === true){
        var getID = URLText.split('buylater=')[1]
        getID = getID.split("&")[0];
        postSend.BuyLaterId = getID
    }
    else if (URLText.includes('id') === true){
        var getID = URLText.split("id=").slice(-1)[0]
        getID = getID.split("&");
        var pasrseID = getID[0].slice(2)
        hotelId = pasrseID.slice(0,-2)
        if (URLText.includes("adult") || URLText.includes("child") || URLText.includes("checkin") || URLText.includes("checkout")){
            setParaFromURLQuery()
        }

    }
    else if(URLText.includes('#') === true){
        var getID = URLText.split("#").slice(-1)[0]
        getID = getID.split("&")[0];
        hotelId = getID
        setParaFromURLQuery()
    }


};

function gobacklist() {
    //go back only came from menu
    const goback = window.location.href.split("list.html");
    window.location.href = goback.slice(2)+"list.html"
}


/**
 * gets the hotel configurations from the server.
 * This would result in couple of function calls;
 *
 *  getHotelconfigAJAXID sends the ID that is given /?id=121223412341432
 *
 *  getHotelConfigAJAXSubdomain sends the Subdomain .xyz/#demo
 *
 *  showSavedReservation sends the buylater ID
 *
 *  all these functions are in a file called AjaxCallsFromServer, and they will result in different calls too.
 */
window.getHotelConfig = function(){

    var URLText = window.location.href;

    if (URLText.includes('resid') === true){
        document.getElementById('menupage').setAttribute('style','display: block')
    }
    else if (URLText.includes('buylater') === true){
        var getID = URLText.split('buylater=')[1]
        getID = getID.split("&")[0];
        showSavedReservation(getID)
        document.getElementById('menupage').setAttribute('style','display: block')
    }
    else if (URLText.includes('id') === true){
        getHotelconfigAJAXID(hotelId)
        document.getElementById('menupage').setAttribute('style','display: block')

    }
    else if(URLText.includes('#') === true){
        getHotelConfigAJAXSubdomain(hotelId)
        document.getElementById('menupage').setAttribute('style','display: block')
        document.getElementById('listOfNavBaritems').removeChild(document.getElementById('gobackButton'))
    }
    else if (hotelId == null || hotelId == ""){
        window.location.href="/list.html"
    }


};

/**
 * This function would call the following functions showCurrencries(), setTime(), setCountOfPeople(), setAttr()
 * it would set the logo, theme, and hotellink and enables the searchbutton
 *
 * @param respones object returned by the functions
 *
 * 1-   getHotelconfigAJAXID
 * 2-   getHotelConfigAJAXsubdomain
 */
function setParameters(respones) {

        // saves the respones from the server to searchConfig

        searchConfig = respones

        if (searchConfig.ThemeNo !== null){
            var theme = parseInt(searchConfig.ThemeNo)
            $("#changestyle").attr("href", 'css/bootstrap.min'+theme+'.css');
        }



        showCurrencries()
        currencyTheUserPicked = "TRY"
        var logo = searchConfig.LogoURL
        document.getElementById('totalPriceCurrency').innerHTML = currencyTheUserPicked


    if (screenWidth < 400){//(window.location.href.indexOf("ios")>-1===true || window.location.href.indexOf("android")>-1===true) {
        if (logo === null){
            var pHotelName = document.createElement('p')
            pHotelName.setAttribute('style','float: left;margin-left: 15px;margin-top: 22px;')
            pHotelName.setAttribute('class','navbar-text')
            pHotelName.innerHTML = searchConfig.HotelName.substring(0,20);
            document.getElementById('hotelLink').appendChild(pHotelName)
        }
        else{
            document.getElementById('hotelLink').innerHTML =  '<img src="'+logo +'" alt="" width="60px" height="60px"/>' + '<p style="float: left;margin-left: 67px;margin-top: -38px;"class="navbar-text">'+searchConfig.HotelName.substring(0,20);+'</p>'
        }
    }
    else{
        if (logo === null){
            var pHotelName = document.createElement('p')
            pHotelName.setAttribute('style','float: left;margin-left: 15px;margin-top: 22px;')
            pHotelName.setAttribute('class','navbar-text')

            pHotelName.innerHTML = searchConfig.HotelName;
            document.getElementById('hotelLink').appendChild(pHotelName)
        }
        else{
            document.getElementById('hotelLink').innerHTML =  '<img src="'+logo +'" alt="" width="60px" height="60px"/>' + '<p style="float: left;margin-left: 67px;margin-top: -38px;"class="navbar-text">'+searchConfig.HotelName;+'</p>'
        }
    }
        document.getElementById('hotelLink').href = "https://www.fastbooking.xyz/index.html?id=12"+searchConfig.HotelID +"12"

        setTime();
        setCountOfPeople();
        setAttr();
        document.getElementById('searchButton').disabled = false;

}


/**
 * setTime()
 * function that will get the time from the searchConfig and would set it to,
 * datePickerCheckInDate  // season, today, mindaystocheckin are the factors that are decide the day
 * datePickerCheckOutDate // datePickerCheckInDate and minLos are the factors that decide the day
 */

function setTime() {

    var today = new moment()
    // gets the values and check if they are valid
    datePickerCheckInDate = (moment(searchConfig.StartOfSeason).isAfter(today) === true) ? moment(searchConfig.StartOfSeason) : today
    var minDaysToCheckIn = (searchConfig.MinDaysToCheckIn === null) ? 0 : (searchConfig.MinDaysToCheckIn)
    var minLOS = (searchConfig.MinLOS === null) ? 1 : searchConfig.MinLOS
    //adds the dates to check in and out

    if(moment(searchConfig.StartOfSeason) < today.add(minDaysToCheckIn)){
        datePickerCheckInDate = today.add(minDaysToCheckIn,'d')
    }

    datePickerCheckOutDate = new moment(datePickerCheckInDate)
    datePickerCheckOutDate.add(minLOS,'d')
}


/**
 * set the amount of people that are allowed to stay in a room,
 * maxPax = the amount of people allowed
 * maxAdult = how many Adults allowed in the room
 * maxChild = how many children allowed in the room
 * set's the tooltip on the Adult and child divs.
 */

function setCountOfPeople() {

    maxPeople.maxPax = (searchConfig.MaxPax !== null) ? searchConfig.MaxPax : 10
    maxPeople.maxAdult = (searchConfig.MaxAdult !== null) ? searchConfig.MaxAdult : 5
    maxPeople.maxChild = (searchConfig.MaxPax !== null) ?  maxPeople.maxPax - maxPeople.maxAdult : 5

    document.getElementById('adult').setAttribute('value', 2);
    document.getElementById('child').setAttribute('min',0);
    document.getElementById('adult').setAttribute('max', maxPeople.maxAdult);
    document.getElementById('adultDiv').title = "Max adults from this pax is " + peopleSelected.adult
    document.getElementById('childDiv').title = "Max Pax is" + maxPeople.maxChild
}


/**
 * setAttr()
 * function that set's the tooltips, datetimepickers, childage, and age pickers
 *
 * Read more about the datetimepicker in the documentation of the library.
 *
 */
function setAttr() {

    var maxChildReason = "Max child is " + maxPeople.maxChild
    var maxAdultReason = "Max Adult is " +maxPeople.maxAdult

    $(document).ready(function(){

        document.getElementById('datetimepicker1').title = reasonToCheckIn
        document.getElementById('datetimepicker2').title = reasonToCheckOut
        document.getElementById('adultDiv').title = maxAdultReason
        document.getElementById('childDiv').title = maxChildReason
        $('[data-toggle="tooltip"]').tooltip();

    });


    $('#datetimepicker1').datetimepicker({

        //Gives the min date to be set
        minDate: datePickerCheckInDate,
        format: 'DD.MM.YYYY',
        ignoreReadonly: true,


    }).on('dp.change',function (e) {

        var minLOS = (searchConfig.MinLOS === null) ? 1 : searchConfig.MinLOS
        var date = moment(document.getElementById('datetimepicker1').value, 'DD.MM.YYYY')
        date.add(minLOS,'d')

        $('#datetimepicker2').data('DateTimePicker').minDate(date)
        $('#datetimepicker2').data('DateTimePicker').date(date)

    });

    $('#datetimepicker2').datetimepicker({
        minDate: datePickerCheckOutDate,
        format: 'DD.MM.YYYY',
        ignoreReadonly: true,
    });


    // creating a dynamic picker for children's age according to the searchConfig that we get from the hotel

    $(".childDiv").ready(function () {
        for (var i = 0; i < 5; i++) {
            var s = $('#chd_AGE'+i+'');

            if (searchConfig.MaxChildAge !== null) {
                for (var j = 0; j < searchConfig.MaxChildAge + 1; j++) {
                    $("<option />", {value: j, text: j}).appendTo(s);
                }
            }
            else{

                for (var j = 0; j < 14; j++) {
                    $("<option />", {value: j, text: j}).appendTo(s);
                }
            }
        }
    });

    $(".childDiv button").click(function() {
        var csi = $("#child").val();
        $('.cage').hide();
        if (csi>0){$(".clbl").show();}
        if ((csi==1) || (csi==2) || (csi==3) || (csi==4) || (csi==5)){$("#chd_AGE0").show();}
        if ((csi==2) || (csi==3) || (csi==4) || (csi==5)){$("#chd_AGE1").show();}
        if ((csi==3) || (csi==4) || (csi==5)){$("#chd_AGE2").show();}
        if ((csi==4) || (csi==5)){$("#chd_AGE3").show();}
        if (csi==5){$("#chd_AGE4").show();}
        if (csi==0){$(".clbl").hide();}
    });

    if (searchConfig.Whatsapp === undefined || searchConfig.Whatsapp === null || searchConfig.Whatsapp === ""){
        document.getElementById('whatsappImgButton').setAttribute('style','display: none')
    }
    if (window.location.href.toLowerCase().includes('checkin') === true || window.location.href.toLowerCase().includes('checkout') === true){
        setDatesFromURLAvailable()
    }

}

/**
 * searching the hotel for rooms searchClicked
 * it takes the dates, the amount of people, and searches according to those inputs by sending the thisURL to
 *  getRoomsAJAXCall() which returns the rooms config that the hotel has
 */
var searchClicked = function () {
    // gets  how many people are there
    var adult = parseInt(document.getElementById('adult').value);
    var child = parseInt(document.getElementById('child').value);
    var sum = adult + child

    // get the checkin and out dates

    var arrayOfDates = GetDates()
    pickeddatePickerCheckInDate = arrayOfDates[0]
    pickeddatePickerCheckOutDate = arrayOfDates[1]

    if (child == 0) {
        child = "";
    }
    extraServicesDivRender()
    peopleSelected.child = child
    peopleSelected.adult = adult

    var sum = peopleSelected.adult + peopleSelected.child

    if (sum <= maxPeople.maxPax){

        // changes the attributes of the datepickers and the search button to be replaced with a different functionality
        setAttrForSearchAgain()
        document.getElementById('searchButton').removeAttribute('onclick')

        var thisURL = "https://api.hoteladvisor.net/Hotel/GetHotelRooms?id=" + hotelId + "&Arrival=" + pickeddatePickerCheckInDate + "&Departure=" + pickeddatePickerCheckOutDate + "&Adult=" + adult + "&ChdAges=" + child + "&Currency=TRY";
        //sends a get request
        getRoomsAJAXCall(thisURL)
    }
    else{
        alert("Selected people doesn't satisfy the Max Pax")
    }
}


/**
 * this function displays the people that will have their names filled.
 *
 * it renders a table in the screen with all the input fields in it.
 */
function amountOfPeopleToShowOnScreen() {

    var tabIndex = 4
    sum = peopleSelected.adult + peopleSelected.child
    var container = document.getElementById("peoplesTable");

    container.innerHTML = '<tr><th id="sex" class="col-md-2 col-xs-3 textLabels" >'+languageDictonary.sex+'</th> <th id="name"  class="textLabels col-md-4 col-xs-4">'+languageDictonary.name+' *</th> <th id="surname" class="textLabels col-md-4 col-xs-5" >'+languageDictonary.surname+' *</th> </tr>'

    for (var i = 1; i <= sum; i++) {

        var sex = (i%2 === 0)? '<select id="sex'+ i +'"size="1" name="cinsiyet" class="form-control" tabindex="' + tabIndex + '" ><option id="sexClassFemale'+i+'" value="K" selected="selected">'+languageDictonary.w+'</option> <option id="sexClassMale'+i+'" value="M">'+languageDictonary.m+'</option> </select>' :'<select id="sex' + i + '"size="1" name="cinsiyet" class="form-control" tabindex="' + tabIndex + '" ><option id="sexClassFemale'+i+'" value="K" >'+languageDictonary.w+'</option> <option id="sexClassMale'+i+'" value="E" selected="selected">'+languageDictonary.m+'</option> </select>'


        container.innerHTML += '<tr> <td class="col-md-2 col-xs-3">'+sex+'</td><td class="col-md-4 col-xs-4"><input id="name' + i + '"type="text" size="15" name="ad" value="" class="form-control" maxlength="20" tabindex="' + tabIndex + '" ></td>'+
        '<td class="col-md-4 col-xs-5"><input id="surname' + i + '" type="text" size="15" name="soyad" value="" class="form-control" maxlength="20" tabindex="' + tabIndex + '" ></td></tr>'


        tabIndex++
    }

    typeOfPayment()

}


/**
 * this would get all the information needed to be sent to the server by a post request
 */
function sendButtonPressed() {

    if (radioValue === 5){//because cc with guarantee is also type 3
        radioValue = 3
    }
    placeExtraServices()
    var surname  = document.getElementById('surname1').value
    postSend.HotelID = hotelId;
    postSend.Arrival = GetDates()[0];
    postSend.Departure = GetDates()[1];
    postSend.Adult = peopleSelected.adult;
    postSend.Currency = roomSelected.currency;
    postSend.RoomTypeId = roomSelected.roomTypeID;
    postSend.RateTypeId = roomSelected.ratetypeId;
    postSend.BoardTypeId = roomSelected.boardtypeId;
    postSend.Price = roomSelected.price;
    postSend.CardCvv = document.getElementById('CVC').value;
    postSend.CardExpires = document.getElementById('expmonth').value +'/' +document.getElementById('expyear').value;

    postSend.CardNo = document.getElementById('pan1').value + '' + document.getElementById('pan2').value + "" + document.getElementById('pan3').value +""+document.getElementById('pan4').value;
    postSend.PaymentType = radioValue;



    if (peopleSelected.child !== null && peopleSelected.child !== "") {

         var numPeople = parseInt(peopleSelected.adult) + parseInt(peopleSelected.child)
    }
    else{

        var numPeople = parseInt(peopleSelected.adult)
    }


    var i =1;
    for (i = 1; i <= numPeople; i++) {

        var peopleToBeAdded = new person(document.getElementById('sex' + i + '').value, null , document.getElementById('name' + i + '').value, document.getElementById('surname' + i + '').value)

        postSend.People.push(peopleToBeAdded);
    }

    var childAges = ""

    for(var i = 0; i < peopleSelected.child; i++){
        childAges += document.getElementById('chd_AGE'+i+'').value + " "
    }

    postSend.ChdAges = childAges
    postSend.Email = document.getElementById('email').value;
    postSend.Phone = document.getElementById('telephone').value;
    postSend.CardHolder = document.getElementById('nameOnCard').value;
    postSend.PortalReferenceId = document.getElementById('refID').value;
    postRequestAJAX(postSend)
    for (i = 1; i <= numPeople; i++) {
        postSend.People.pop()
    }
}

//saves,creates and deletes cookies
/*function createCookie(n,t,i){var r,u;i?(r=new Date,r.setTime(r.getTime()+i*864e5),u="; expires="+r.toGMTString()):u="";document.cookie=n+"="+t+u+"; path=/"}
function readCookie(n){for(var t,r=n+"=",u=document.cookie.split(";"),i=0;i<u.length;i++){for(t=u[i];t.charAt(0)==" ";)t=t.substring(1,t.length);if(t.indexOf(r)==0)return t.substring(r.length,t.length)}return null}
function eraseCookie(n){createCookie(n,"",-1)}*/

function createCookie(n,t,i){
    var mob = (window.location.href.indexOf("android")>-1);
    if (mob==false) {
        var r,u;i?(r=new Date,r.setTime(r.getTime()+i*864e5)
        ,u="; expires="+r.toGMTString()):u="";document.cookie=n+"="+t+u+"; path=/"}
    else {
        window.localStorage.setItem("cookie", n+"="+t+u+"; path=/");
    }
}

function readCookie(n){for(var t,r=n+"=",u=document.cookie.split(";"),i=0;i<u.length;i++){for(t=u[i];t.charAt(0)==" ";)t=t.substring(1,t.length);if(t.indexOf(r)==0)return t.substring(r.length,t.length)}return null}

function eraseCookie(n){createCookie(n,"",-1)}


var make = false

// DATE DIFFERENCE FUNCTION finds the difference between two dates
function getDifferenceCheckIn_Out() {
    // Discard the time and time-zone information.
    inFunctionCheckIn = document.getElementById('datetimepicker1').value;
    inFunctionCheckOut = document.getElementById('datetimepicker2').value;
    var now = moment(inFunctionCheckOut, "DD.MM.YYYY"); //todays date
    var end = moment(inFunctionCheckIn, "DD.MM.YYYY"); // another date
    var duration = moment.duration(now.diff(end));
    var days = duration.asDays();

    return days
}

// AUTO TAB FUNCTION give the user the second tab of the credit card

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

$('.pan').on('keyup', function(e){
    // get keycode of current keypress event
    var code = (e.keyCode || e.which);

    // do nothing if it's an arrow key
    if(code == 37 || code == 38 || code == 39 || code == 40) {
    }
    else{
        var value =document.getElementById(''+e.target.id+'').value

         if (value.length == 4){
             var panNumber = parseInt(e.target.id.split('')[3])
             panNumber++
             if (panNumber === 5){

             }
             else{
                 document.getElementById('pan'+panNumber+'').focus()
             }

         }
    }
});

//CREDIT CARD YEAR MONTH creates the dates for the credit card

$(document).ready(function () {
    var s = $('#expmonth')
    for(var i=1; i < 13; i++) {
        $("<option />", {value: i, text: i}).appendTo(s);
    }
    var year = new Date()
    s = $('#expyear')
    year = year.getFullYear()
    for(var i=1; i < 21; i++) {
        $("<option />", {value: year, text: year}).appendTo(s);
        year++
    }
})



function changeLan(x) {
    selectedLang = x
    languageDictonary = setLanguageAndReturnDictonary(x,differenceBetweenCheckInAndOut,peopleSelected,rooms,roomIsChoosen,resposeForShowReservation)
    eraseCookie('languageCookie')
    createCookie('languageCookie',x,1/24);
}

$(document).ready(function() {
    $('#register').attr('disabled',true);

});

function checkButton() {

    var checker = true;
    var done = false;
    var highlight = true;
    var firstName = document.getElementById('name1').value;
    var lastName = document.getElementById('surname1').value;
    var email = document.getElementById('email').value;
    var telephone = document.getElementById('telephone').value;
    var pan1 = document.getElementById('pan1').value;
    var pan2 = document.getElementById('pan2').value;
    var pan3 = document.getElementById('pan3').value;
    var pan4 = document.getElementById('pan4').value;
    var cvv = document.getElementById('CVC').value;
    var nameOnCard = document.getElementById('nameOnCard').value

    var arraylist = ['name1','surname1','email','telephone','pan1','pan2','pan3','pan4','CVC','nameOnCard']
    var array = [firstName,lastName,email,telephone,pan1,pan2,pan3,pan4,cvv,nameOnCard]

    if (radioValue === 3 || radioValue === 2){
         arraylist = ['name1','surname1','email','telephone']
         array = [firstName,lastName,email,telephone]
    }

    for (var i = 0; i < array.length; i++){
        if (array[i] === "" || array[i] === null){
            checker = valid(arraylist[i])

            if (highlight){
                $('html,body').animate({ scrollTop: $('#'+arraylist[i]+'').offset().top-641}, 700);
                highlight = false
            }
            done = false
        }
        else if (checker){
            alreadyFilled(arraylist[i])
            done = true
        }

    }
    var ccNumber = pan1 + '' + pan2 + '' + '' + pan3 + '' +pan4


    if(done === true && parseInt(telephone)){

        if (valid_credit_card(ccNumber)){
            $('#register').attr('disabled',false);

        }
        else{
            $('#terms').attr('checked', false);
            alert("Credit Card Number is invalid!")
        }

    }
    else if (isNaN(telephone)){
        $('#terms').attr('checked', false);
        alert("Not a phone number")
    }
    else{
        $('#terms').attr('checked', false);
    }

}

function valid(x) {
    document.getElementById(''+x+'').style = '-webkit-box-shadow:inset 0 -1px 0 red !important;box-shadow:inset 0 -1px 0 red !important;'
    if (x === 'CVC'){
        document.getElementById('CVC').style = 'text-align: center !important; -webkit-box-shadow:inset 0 -1px 0 red !important;box-shadow:inset 0 -1px 0 red !important;'
    }
    $('#terms').attr('checked', false);
    return false
}

function alreadyFilled(x){

    document.getElementById(''+x+'').removeAttribute('style')
    document.getElementById(''+x+'').style = '-webkit-box-shadow:inset 0 -1px 0 #ddd !important;box-shadow:inset 0 -1px 0 #ddd !important;'
    if (x === 'CVC'){
        document.getElementById('CVC').style += '-webkit-box-shadow:inset 0 -1px 0 #ddd !important;box-shadow:inset 0 -1px 0 #ddd !important; text-align: center !important;'
    }
}

function searchAgain(){

    var adult = parseInt(document.getElementById('adult').value);
    var child = parseInt(document.getElementById('child').value);

    // get the checkin and out dates

    if (child == 0) {
        child = "";
    }

    peopleSelected.child = child
    peopleSelected.adult = adult

    var sum = peopleSelected.adult + peopleSelected.child


    var arrayOfDates = GetDates()
    pickeddatePickerCheckInDate = arrayOfDates[0]
    pickeddatePickerCheckOutDate = arrayOfDates[1]

    if (sum <= maxPeople.maxPax) {
        var thisURL = "https://api.hoteladvisor.net/Hotel/GetHotelRooms?id=" + hotelId + "&Arrival=" + pickeddatePickerCheckInDate + "&Departure=" + pickeddatePickerCheckOutDate + "&Adult=" + peopleSelected.adult + "&ChdAges=" + peopleSelected.child + "&Currency=TRY";

        document.getElementById('searchButton').removeAttribute('onclick')
        getRoomsAJAXCall(thisURL)
    }

}

function setAttrForSearchAgain() {

    $('#datetimepicker1').datetimepicker({

        //Gives the min date to be set
        minDate: datePickerCheckInDate,
        format: 'DD.MM.YYYY',
        ignoreReadonly: true,


    }).on('dp.change',function (e) {
        var minLOS = (searchConfig.MinLOS === null) ? 1 : searchConfig.MinLOS
        var date = moment(document.getElementById('datetimepicker1').value, 'DD.MM.YYYY')
        date.add(minLOS,'d')

        $('#datetimepicker2').data('DateTimePicker').minDate(date)
        $('#datetimepicker2').data('DateTimePicker').date(date)

        var roomClassDelete = document.getElementById('roomsClassDelete')

        while (roomClassDelete.hasChildNodes()) {
            roomClassDelete.removeChild(roomClassDelete.lastChild);
        }

        var renderAfterPick = document.getElementById('renderAfterPick')

        while (renderAfterPick.hasChildNodes()) {
            renderAfterPick.removeChild(renderAfterPick.lastChild);
        }

        document.getElementById('searchButton').setAttribute('onclick', 'searchAgain()')

        $('.roomPanel').css('display','none')
        $(".guestPanel").css("display", "none");
        $('#roomHeader').removeClass('active');
        $('#paymentHeader').removeClass('active');
        $('#searchHeader').addClass('active')

    });

    $('#datetimepicker2').datetimepicker({
        minDate: datePickerCheckOutDate,
        format: 'DD.MM.YYYY',
        ignoreReadonly: true,
    }).on('dp.change',function (e) {

        var roomClassDelete = document.getElementById('roomsClassDelete')

        while (roomClassDelete.hasChildNodes()) {
            roomClassDelete.removeChild(roomClassDelete.lastChild);
        }

        var renderAfterPick = document.getElementById('renderAfterPick')

        while (renderAfterPick.hasChildNodes()) {
            renderAfterPick.removeChild(renderAfterPick.lastChild);
        }

        document.getElementById('searchButton').setAttribute('onclick', 'searchAgain()')

        $('.roomPanel').css('display','none')
        $(".guestPanel").css("display", "none");
        $('#roomHeader').removeClass('active');
        $('#paymentHeader').removeClass('active');
        $('#searchHeader').addClass('active')

    });
}

function openFindReservation() {

    document.getElementById('fullHTML').classList.add('blur')
    document.getElementById('floatingPayment').setAttribute('style','display: block !important')

}

function getReservation() {
    var resID = document.getElementById('idFindReservation').value
    var surname = document.getElementById('surnameFindReservation').value
    $.ajax({
        type: "GET",
        url: 'https://api.hoteladvisor.net/Hotel/CheckReservation?resid='+resID+'&surname=' + surname,
        success: function (respones) {
            fillInReservationCompletedOrShow(respones)

        }
    });
}

function fillInReservationCompletedOrShow(fillInRoomsResp){
    resposeForShowReservation = fillInRoomsResp
    document.getElementById('paymentDone').removeAttribute('style')
    document.getElementById('findMyReservation').setAttribute('style','display: none')

    var child = (fillInRoomsResp.ChildCount === null)? "" : fillInRoomsResp.ChildCount

    var checkIn = new moment(fillInRoomsResp.Checkin, "YYYY-MM-DD")
    var checkOut = new moment(fillInRoomsResp.Checkout, "YYYY-MM-DD")

    var checkInMonth = checkIn.get('month') + 1
    var checkOutMonth = checkOut.get('month') + 1
    var checkIndate = checkIn.get('date') + '.' + checkInMonth + '.' + checkIn.get('year')
    var checkOutdate = checkOut.get('date') + '.' + checkOutMonth + '.' + checkOut.get('year')



    document.getElementById('acceptPayment').innerHTML  = (fillInRoomsResp.ID===null)? "" : languageDictonary.ConfID +': '
    document.getElementById('namePayment').innerHTML =  (fillInRoomsResp.GuestFullName===null)? "" : languageDictonary.name +': '
    document.getElementById('checkInPayment').innerHTML = (fillInRoomsResp.Checkin===null)? "" : languageDictonary.CI +': '
    document.getElementById('checkoutPayment').innerHTML = (fillInRoomsResp.Checkout===null)? "" : languageDictonary.CO +': '

    document.getElementById('peopleInPayment').innerHTML= (fillInRoomsResp.AdultCount===null)? "" : languageDictonary.adult + ' - '+ languageDictonary.C +': '
    document.getElementById('roomTypeInPayment').innerHTML= (fillInRoomsResp.RoomType===null)? "" :languageDictonary.roomtype +': '

    document.getElementById('paymentType').innerHTML= (fillInRoomsResp.RateType===null)? "" : languageDictonary.rateType +': '
    document.getElementById('boardTypeInPayment').innerHTML= (fillInRoomsResp.BoardType===null)? "" : languageDictonary.boardType +': '
    document.getElementById('specialreqInPayment').innerHTML= (fillInRoomsResp.SpecialRequests===null)? "" : languageDictonary.specialreq +': '
    document.getElementById('paymentType').innerHTML= (fillInRoomsResp.PaymentType===null || fillInRoomsResp.PaymentType === "" )? "" : languageDictonary.paymentType +': '

    document.getElementById('cancellationPolicy').innerHTML= (fillInRoomsResp.CancelPolicy===null)? "" : languageDictonary.cancelPoly +': '
    document.getElementById('hotelAdres').innerHTML=(fillInRoomsResp.HotelAddress===null)? "" : languageDictonary.adres +': '
    document.getElementById('hotelPhone').innerHTML= (fillInRoomsResp.HotelPhone===null)? "" : languageDictonary.hotelNumber + ': '

    document.getElementById('acceptPaymentRequest').innerHTML  = (fillInRoomsResp.ID===null)? "" : fillInRoomsResp.ID
    document.getElementById('namePaymentRequest').innerHTML =  (fillInRoomsResp.GuestFullName===null)? "" :  fillInRoomsResp.GuestFullName
    document.getElementById('checkInPaymentRequest').innerHTML = (fillInRoomsResp.Checkin===null)? "" :  checkIndate
    document.getElementById('checkoutPaymentRequest').innerHTML = (fillInRoomsResp.Checkout===null)? "" :  checkOutdate

    document.getElementById('peopleInPaymentRequest').innerHTML= (fillInRoomsResp.AdultCount===null)? "" :  fillInRoomsResp.AdultCount +' - '+ child
    document.getElementById('roomTypeInPaymentRequest').innerHTML= (fillInRoomsResp.RoomType===null)? "" :  fillInRoomsResp.RoomType

    document.getElementById('paymentTypeRequest').innerHTML= (fillInRoomsResp.RateType===null)? "" : fillInRoomsResp.RateType
    document.getElementById('boardTypeInPaymentRequest').innerHTML= (fillInRoomsResp.BoardType===null)? "" : fillInRoomsResp.BoardType
    document.getElementById('specialreqInPaymentRequest').innerHTML= (fillInRoomsResp.SpecialRequests===null)? "" : fillInRoomsResp.SpecialRequests
    document.getElementById('paymentTypeRequest').innerHTML= (fillInRoomsResp.PaymentType===null || fillInRoomsResp.PaymentType === "" )? "" : fillInRoomsResp.PaymentType

    document.getElementById('cancellationPolicyRequest').innerHTML= (fillInRoomsResp.CancelPolicy===null)? "" : fillInRoomsResp.CancelPolicy
    document.getElementById('hotelAdresRequest').innerHTML=(fillInRoomsResp.HotelAddress===null)? "" :  fillInRoomsResp.HotelAddress
    document.getElementById('hotelPhoneRequest').innerHTML= (fillInRoomsResp.HotelPhone===null)? "" : fillInRoomsResp.HotelPhone

    var mapImage = (fillInRoomsResp.HotelLatitude === null || fillInRoomsResp.HotelLongitude === null) ? "" : 'https://maps.googleapis.com/maps/api/staticmap?center='+fillInRoomsResp.HotelLatitude+','+fillInRoomsResp.HotelLongitude+'8&zoom=11&size=250x250&key=AIzaSyCc3RcA6Gy1Yf3fbHeLpB2blgPaGZVvUX8' ;
    mapLink = (fillInRoomsResp.HotelLatitude === null || fillInRoomsResp.HotelLongitude === null) ? "" : 'https://www.google.co.uk/maps/place/'+fillInRoomsResp.HotelLatitude+','+fillInRoomsResp.HotelLongitude

    if (mapImage !== null && mapImage !== ""){
        var infoImg = document.getElementById('mapHotelIMG')
        infoImg.setAttribute('onclick','openLink()')
        infoImg.setAttribute('src',mapImage)
        infoImg.setAttribute('width','250px')
        infoImg.setAttribute('height','250px')
    }

}

function closeFloatingPaymentPage() {
    document.getElementById('findMyReservation').removeAttribute('style')
    document.getElementById('fullHTML').classList.remove('blur')
    document.getElementById('floatingPayment').setAttribute('style','display: none !important')
    document.getElementById('paymentDone').setAttribute('style','display: none')
    document.getElementById('idFindReservation').value = ""
    document.getElementById('surnameFindReservation').value = ""
}


function changeCurrency(x) {
    if (rooms !== null){
        if (roomIsChoosen !== null){changePickedRoomcurrency(x)}else{renderCurrencies(x,rooms);document.getElementById('totalPriceCurrency').innerHTML = x}
    }
    currencyTheUserPicked = x
    document.getElementById('currencyHolder').innerHTML = currencyTheUserPicked + ' <span class="caret"></span>'
    document.getElementById('totalPriceCurrency').innerHTML = x
}

function openLink() {
    window.open(mapLink)
}

function valid_credit_card(value) {
    // accept only digits, dashes or spaces
    if (/[^0-9-\s]+/.test(value)) return false;

    // The Luhn Algorithm. It's so pretty.
    var nCheck = 0, nDigit = 0, bEven = false;
    value = value.replace(/\D/g, "");

    for (var n = value.length - 1; n >= 0; n--) {
        var cDigit = value.charAt(n),
            nDigit = parseInt(cDigit, 10);

        if (bEven) {
            if ((nDigit *= 2) > 9) nDigit -= 9;
        }

        nCheck += nDigit;
        bEven = !bEven;
    }

    return (nCheck % 10) == 0;
}

$("input[type='radio']").click(function(){
    radioValue = parseInt($("input[name='optradio']:checked").val());
    if (radioValue === 1){//cc  1-cc 2-wire 3-payatotel 4-payatotelwithcc
        document.getElementById('wireTransferHolder').style = 'display: none'
        document.getElementById('creditCardChargePolicy').setAttribute('style','visibilty: visible')
        document.getElementById('CreditCardDiv').removeAttribute('style')
        document.getElementById('CreditCardDiv').setAttribute('style','display: block')
        document.getElementById('creditCardChargePolicy').innerHTML = languageDictonary.ccNeededNow

    }
    else if (radioValue === 2){//with wire

        document.getElementById('wireTransferHolder').style = 'display: block'
        document.getElementById('wireTransfer').innerHTML = searchConfig.BankInformation
        document.getElementById('CreditCardDiv').removeAttribute('style')
        document.getElementById('CreditCardDiv').setAttribute('style','display: none')

    }
    else if (radioValue == 5){//at hotel with guarentee
        document.getElementById('wireTransferHolder').style = 'display: none'
        document.getElementById('creditCardChargePolicy').innerHTML = languageDictonary.ccNeededLater
        document.getElementById('CreditCardDiv').removeAttribute('style')
        document.getElementById('CreditCardDiv').setAttribute('style','display: block')
        document.getElementById('creditCardChargePolicy').setAttribute('style','visibilty: visible')
        document.getElementById('CreditCardDiv').removeAttribute('style')

    }
    else if(radioValue == 4){//py by down payment
        document.getElementById('wireTransferHolder').style = 'display: none'
        document.getElementById('creditCardChargePolicy').setAttribute('style','visibilty: visible')
        document.getElementById('CreditCardDiv').removeAttribute('style')
        document.getElementById('CreditCardDiv').setAttribute('style','display: block')
        document.getElementById('creditCardChargePolicy').innerHTML = languageDictonary.ccNeededDownPayment

    }
    else{ //pay at hotel
        document.getElementById('wireTransferHolder').style = 'display: none'
        document.getElementById('CreditCardDiv').removeAttribute('style')
        document.getElementById('CreditCardDiv').setAttribute('style','display: none')
        document.getElementById('creditCardChargePolicy').setAttribute('style','display: visible')

    }
});

function typeOfPayment(){

    document.getElementById('creditCardChargePolicy').setAttribute('style','visibility: hidden;')
    radioValue = 1

    // this would show the hotel payment configuration..
    if (searchConfig.PayAtHotel === false){
        document.getElementById('payLaterRadio').setAttribute('style','display: none')
        document.getElementById('payLaterLabel').setAttribute('style','display: none')
    }

    if (searchConfig.PayByWire === false){
        document.getElementById('payByWireRadio').setAttribute('style','display: none')
        document.getElementById('payBywireLabel').setAttribute('style','display: none')
    }

    if (searchConfig.PayByCC === false){
        document.getElementById('payByCreditRadio').setAttribute('style','display: none')
        document.getElementById('paybyCCLabel').setAttribute('style','display: none')
    }

    if (searchConfig.PayAtHotelWithCCGuarantee === false){
        document.getElementById('payLaterWithGuaranteeLabel').setAttribute('style','display: none')
        document.getElementById('payLaterWithGuaranteeRadio').setAttribute('style','display: none')
    }

    if (searchConfig.PayByDownPayment === false){
        document.getElementById('payByDownPaymentLabel').setAttribute('style','display: none')
        document.getElementById('payByDownPaymentRadio').setAttribute('style','display: none')
    }

    if (searchConfig.PayAtHotelWithCCGuarantee === false && searchConfig.PaybyCC === false && searchConfig.PaybyWire === false){
        document.getElementById('paymentForm').setAttribute('style','display: none')
        radioValue = 3
    }

    // document.getElementById('CreditCardDiv').setAttribute('style','display: none')
    translateCCinfo()
}


function readText(){
    getFromTextFile()
    document.getElementById('fullHTML').classList.add('blur')
    document.getElementById('floatingScreenForAgreement').setAttribute('style','display: block !important')
    document.getElementById('floatingAgreement').setAttribute('style','display: block !important')
}


function getFromTextFile() {
    var allText;
    // var file = 'txt/'+selectedLang+'.txt'
    var file = 'txt/tr.txt';
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);

    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200 || rawFile.status === 0)
            {
                allText = rawFile.responseText;
                if (allText !== null){

                    document.getElementById('floatingAgreement').innerHTML = decodeURI(allText);
                    //document.getElementById('floatingAgreement').innerHTML = document.getElementById('floatingAgreement').innerHTML.replace('@day','09');


                    const d = new Date();
                    const getDate = d.getDate();
                    const getMonth = d.getMonth();
                    const getYear = d.getFullYear();
                    document.getElementById('floatingAgreement').innerHTML = document.getElementById('floatingAgreement').innerHTML.replace('@day', getDate);
                    document.getElementById('floatingAgreement').innerHTML = document.getElementById('floatingAgreement').innerHTML.replace('@month', getMonth+1); //0=January, 1=February ...
                    document.getElementById('floatingAgreement').innerHTML = document.getElementById('floatingAgreement').innerHTML.replace('@year', getYear);

                    document.getElementById('floatingAgreement').innerHTML = document.getElementById('floatingAgreement').innerHTML.replace('@otelname',searchConfig.HotelName);
                    document.getElementById('floatingAgreement').innerHTML = document.getElementById('floatingAgreement').innerHTML.replace('@oteltel', searchConfig.Phone);
                    document.getElementById('floatingAgreement').innerHTML = document.getElementById('floatingAgreement').innerHTML.replace('@otelmail', searchConfig.Email);

                    if(document.getElementById('name1').value === null || document.getElementById('surname1').value === null || document.getElementById('telephone').value === null || document.getElementById('email').value === null || document.getElementById('name1').value === "" || document.getElementById('surname1').value === "" || document.getElementById('telephone').value === "" || document.getElementById('email').value === ""){
                        document.getElementById('floatingAgreement').innerHTML = document.getElementById('floatingAgreement').innerHTML.replace('@customername', 'Lütfen Bilgilerinizi Giriniz');
                        document.getElementById('floatingAgreement').innerHTML = document.getElementById('floatingAgreement').innerHTML.replace('@customertel', 'Lütfen Bilgilerinizi Giriniz');
                        document.getElementById('floatingAgreement').innerHTML = document.getElementById('floatingAgreement').innerHTML.replace('@customermail', 'Lütfen Bilgilerinizi Giriniz');
                    }
                    else{
                        document.getElementById('floatingAgreement').innerHTML = document.getElementById('floatingAgreement').innerHTML.replace('@customername', document.getElementById('name1').value+' '+document.getElementById('surname1').value);
                        document.getElementById('floatingAgreement').innerHTML = document.getElementById('floatingAgreement').innerHTML.replace('@customertel', document.getElementById('telephone').value);
                        document.getElementById('floatingAgreement').innerHTML = document.getElementById('floatingAgreement').innerHTML.replace('@customermail', document.getElementById('email').value);
                    }


                    document.getElementById('floatingAgreement').innerHTML = document.getElementById('floatingAgreement').innerHTML.replace('@checkin', document.getElementById('datetimepicker1').value );
                    document.getElementById('floatingAgreement').innerHTML = document.getElementById('floatingAgreement').innerHTML.replace('@checkout', document.getElementById('datetimepicker2').value);
                    document.getElementById('floatingAgreement').innerHTML = document.getElementById('floatingAgreement').innerHTML.replace('@numofperson', peopleSelected.adult);
                    document.getElementById('floatingAgreement').innerHTML = document.getElementById('floatingAgreement').innerHTML.replace('@childage', getChildages);
                    document.getElementById('floatingAgreement').innerHTML = document.getElementById('floatingAgreement').innerHTML.replace('@roomtype', roomSelected.roomtype);
                    document.getElementById('floatingAgreement').innerHTML = document.getElementById('floatingAgreement').innerHTML.replace('@ratetype', roomSelected.ratetype);
                    document.getElementById('floatingAgreement').innerHTML = document.getElementById('floatingAgreement').innerHTML.replace('@boardtype', translateBoardType(roomSelected.boardtype));
                    document.getElementById('floatingAgreement').innerHTML = document.getElementById('floatingAgreement').innerHTML.replace('@totalpayment', roomSelected.price);
                    document.getElementById('floatingAgreement').innerHTML = document.getElementById('floatingAgreement').innerHTML.replace('@paymenttype', translatePaymentType(radioValue));
                    document.getElementById('floatingAgreement').innerHTML = document.getElementById('floatingAgreement').innerHTML.replace('@cancelpoly', roomSelected.cancelPolicy);

                }

            }
        }
    }

    rawFile.send(null);
}

function closeFloatingAgreemntPage() {
    document.getElementById('fullHTML').classList.remove('blur')
    document.getElementById('floatingScreenForAgreement').removeAttribute('style')
}

function translatePaymentType(x){
    switch (x){
        case 1:
            return picked.payCC
        case 2:
            return picked.payWire
        case 3:
            return picked.paylater
        case 4:
            return picked.payDownPayment
        case 5:
            return picked.payLaterGarNeeded
        default:
            return x
    }
}

function getChildages(){

    if(document.getElementById('chd_AGE0').value === '0'){
        return '-';
    }
    else{
        var childsAges = document.getElementById('chd_AGE0').value;

        for(var i = 1; i < peopleSelected.child; i++){
            childsAges += ", " + document.getElementById('chd_AGE'+i+'').value
        }
        return childsAges;
    }
}

function openWhatsapp(){

    if (searchConfig.Whatsapp !== undefined && searchConfig.Whatsapp !== null){
        var phoneNumber = searchConfig.Whatsapp.split(' ').join('')
        window.location = "https://api.whatsapp.com/send?phone=" + phoneNumber
    }
}
var displayTab = 1
function displayExtraInformation() {
    if (displayTab === 1){
        document.getElementById('extrainfoTab').setAttribute('style','display: block')
        displayTab= 0
    }
    else{
        document.getElementById('extrainfoTab').setAttribute('style','display: none')
        displayTab = 1
    }
}


function toBeSentServices(ExtraServiceId,Quantity){
    this.ExtraServiceId = ExtraServiceId;
    this.Quantity = Quantity
}


function setParaFromURLQuery() {

    document.getElementById('adult').value= (qs('Adult') === "") ? document.getElementById('adult').value : qs('Adult')
    document.getElementById('child').value= (qs('HotelChild') === "") ? document.getElementById('child').value : qs('HotelChild')

    var csi = $("#child").val();
    if (csi > 0) {
        $(".clbl").show();
    }
    for (var i = 0; i < qs('HotelChild'); i++) {

        $('#chd_AGE'+i+'').show();
        $('#chd_AGE' + i + '').val()
    }
    for (var i = 5; i >= qs('HotelChild'); i--){
        $('#chd_AGE'+i+'').hide();
    }
}

function qs(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)")
    var results
    if (window.location.href.includes('#')){
        x = window.location.href.split("#");
        results = regex.exec(x[1]);
    }
    else{
        results = regex.exec(location.search);
    }

    return results === null ? "" : decodeURIComponent(results[1]);
}

function setDatesFromURLAvailable(){

    var checkIn = new moment(qs('CheckIn'))
    var checkOut = new moment(qs('CheckOut'))
    $('#datetimepicker1').data('DateTimePicker').date(checkIn)
    $('#datetimepicker2').data('DateTimePicker').date(checkOut)

}


function openCalllistPage() {
    if (window.location.href.includes('#')){
        const goback = window.location.href.split("#");
        window.location.href = goback[0]+"calllist.html?id=12" + hotelId + '12'
    }
    else{

        var getID = window.location.href.split("?id=").slice(-1)[0]
        getID = getID.split("&");
        const goback = window.location.href.split("?id=");
        window.location.href = goback[0]+"calllist.html?id=" + getID[0]

    }
}


var savedServices = null
function extraServicesDivRender(){
    savedServices = searchConfig.AvailableServices
    var container = document.getElementById("extraServicesDiv");

    for (var i = 0; i < savedServices.length; i++){

        var thisServies = savedServices[i]

        container.innerHTML += '<div class="col-xs-12" id='+thisServies.ExtraServiceId+'> <span> <h6> <b>'+thisServies.Name+'</b> </h6> </span> </div>'+
        '<div> <span class="col-xs-5">'+thisServies.Description+'</span> <span class="col-xs-5"> <button class="btnsForSer"  onclick="SubtractServ('+i+')">-</button>'+
        '<span><b class="thisSer" id="thisSer'+i+'">0</b></span> <button class="btnsForSer" onclick="addServ('+i+')">+</button> </span> <span id="numberOfServ'+i+'" class="col-xs-2">0</span></div>'
    }
}


var extrasHash = new HashTable()

function SubtractServ(x){
    if (parseInt(document.getElementById('thisSer'+x+'').innerHTML) !== 0){
        document.getElementById('thisSer'+x+'').innerHTML = parseInt(document.getElementById('thisSer'+x+'').innerHTML) - 1
        document.getElementById('numberOfServ'+x+'').innerHTML = parseInt(document.getElementById('numberOfServ'+x+'').innerHTML) - savedServices[x].UnitPrice
        document.getElementById('totalPrice').innerHTML = parseInt(document.getElementById('totalPrice').innerHTML) - savedServices[x].UnitPrice
        extrasHash.add(savedServices[x].ExtraServiceId, document.getElementById('thisSer'+x+'').innerHTML)
        if (extrasHash.search(savedServices[x].ExtraServiceId) === '0'){
            extrasHash.remove(savedServices[x].ExtraServiceId)
        }

    }
}

function addServ(x){

    document.getElementById('thisSer'+x+'').innerHTML = parseInt(document.getElementById('thisSer'+x+'').innerHTML) + 1
    document.getElementById('numberOfServ'+x+'').innerHTML = parseInt(document.getElementById('numberOfServ'+x+'').innerHTML) + savedServices[x].UnitPrice
    document.getElementById('totalPrice').innerHTML = parseInt(document.getElementById('totalPrice').innerHTML) + savedServices[x].UnitPrice
    extrasHash.add(savedServices[x].ExtraServiceId, document.getElementById('thisSer'+x+'').innerHTML)
}


function placeExtraServices(){
    if (savedServices !== null) {
        for (var i = 0; i < savedServices.length; i++) {
            if (extrasHash.search(savedServices[i].ExtraServiceId)) {
                var x = new toBeSentServices(savedServices[i].ExtraServiceId, extrasHash.search(savedServices[i].ExtraServiceId))
                postSend.Extras.push(x)
            }
        }
    }
}


