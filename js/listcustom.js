var app = angular.module('myApp', []); 
app.controller('hotelCtrl', function ($scope, $http) { 

$scope.hotelname="OTEL ADI"
$scope.cityname="ŞEHİR" 
$scope.searchtext="Otel Adı ya da Bölgesini Yazınız" 

sendRequest("NAME") 

function sendRequest(param){	
$http.get("/json.asp").success(function(data) {
	$scope.hotels = data;
	$scope.order = param;
	angular.forEach($scope.hotels, function (hotels) {		
			hotels.CITY=hotels.CITY.replace(", Turkey","")		
			hotels.CITY=hotels.CITY.toLowerCase()
			hotels.NAME=hotels.NAME.toLowerCase()
	}); 
	
	$scope.startsWith = function (actual, expected) {
    var lowerStr = (actual + "").toLowerCase();
    return lowerStr.indexOf(expected.toLowerCase()) === 0;
    };
	
	return lang = param
});
}
$(".selectLangTr").click(function(){	
$scope.hotelname="OTEL ADI"
$scope.cityname="ŞEHİR" 
$scope.searchtext="Otel Adı ya da Bölgesini Yazınız" 
sendRequest(lang)
})
$(".selectLangEn").click(function(){	
$scope.hotelname="HOTEL NAME"
$scope.cityname="CITY"	
$scope.searchtext="Name or Location of Hotel" 
sendRequest(lang)
});
$(".selectLangDe").click(function(){	
$scope.hotelname="HOTELNAME"
$scope.cityname="STADT"	
$scope.searchtext="Name oder Lage des Hotels" 
sendRequest(lang)
});
$(".selectLangRu").click(function(){	
$scope.hotelname="ГОСТИНИЦА"
$scope.cityname="ГОРОД"	
$scope.searchtext="Имя или расположение отеля" 
sendRequest(lang)
});
$(".selectLangAr").click(function(){	
$scope.hotelname="اسم فندق"
$scope.cityname="مدينة"	
$scope.searchtext="اسم أو موقع الفندق" 
sendRequest(lang)
});
$(".nameorder").click(function(){var ord=this.id;sendRequest(ord)});

});