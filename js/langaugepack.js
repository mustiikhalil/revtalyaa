/**
 * Created by justablaze on 7/9/17.
 */

let englishLanguage = {
    w: 'Female',
    m: 'Male',
    bd: 'Birth Date',
    name: 'Name',
    surname: 'Surname',
    adult: 'Adult',
    A: 'Adult',
    C: 'Child',
    sex: 'Gender',
    Email: "Email",
    phone: 'Telephone Number',
    note: 'Notes for the Hotel',
    cl: 'New Search',
    srch: 'Search',
    CI: 'Check In',
    CO: 'Check Out',
    CAge: 'Child Age',
    N: "Night",
    SO: 'Sold Out',
    rooms: 'Rooms',
    contactInfo: 'Contact Information',
    traveler: 'Travelers',
    payInfo: 'Payment Information',
    CC: 'Credit Card Number',
    dateEX: 'Expire Date',
    CH: 'Name Surname',
    buy: 'Buy',
    Refundable: 'Refundable',
    nonRefund: 'Not Refundable',
    HB: 'Half Board',
    BB: 'Bed & Breakfast',
    AI: 'All Inclusive',
    RO: 'Room Only',
    FB: 'Full Board',
    FBP: 'Full Board Plus',
    UAI: 'Ultra All Inclusive',
    tc: 'I agree on the following terms and conditions',
    room: 'ROOM',
    standard: "STANDARD",
    deluxe: "DELUXE",
    premium: "PREMIUM",
    de: 'German',
    en: 'English',
    ru: 'Russian',
    tr: 'Turkish',
    ar: 'Arabic',
    ccNeededLater: 'Credit card information is taken for guarantee purposes it will NOT BE CHARGED',
    ccNeededNow: 'Credit card will be charged NOW',
    ccNeededDownPayment: 'Credit card will be charged now with 25% down payment',
    resID: 'Reservation ID',
    ConfID: 'Confirmation ID',
    roomtype: 'Room type',
    hotelNumber: "Hotel Phone Number",
    boardType: 'Board Type',
    specialreq: 'Special Request',
    paymentType: "Payment Type",
    cancelPoly: 'Cancellation Policy',
    adres: 'Address',
    rateType: 'Rate Type',
    saveReservation: 'Save Reservation',
    theme: 'Theme',
    paylater: 'Pay At Hotel',
    payCC: 'Pay by Credit Card',
    payWire: 'Pay by Bank transfer',
    payLaterGarNeeded: 'Pay at Hotel with Credit Card guarantee',
    payDownPayment: 'Pay by Down Payment',
    callME: 'Call Me',
    extra: 'Extra Information'

}


let turkishLanguage = {
    w: 'Bayan',
    m: 'Bay',
    bd: 'Doğum Tarihi',
    name: 'Ad',
    surname: 'Soyad',
    adult: 'Kişi',
    A: 'Yetişkin',
    C: 'Çocuk',
    sex: 'Cinsiyet',
    Email: "E-posta",
    phone: 'Telefon',
    note: 'Otel notları',
    cl: 'Yeni Ara',
    CI: 'Giriş Tarihi',
    CO: 'Çıkış Tarihi',
    srch: 'Ara',
    CAge: 'Çocuk Yaşları',
    N: "Gece",
    SO: 'Satıldı',
    rooms: 'Odalar',
    contactInfo: 'İletişim bilgileri',
    traveler: 'Misafir Bilgileri',
    payInfo: 'Ödeme Bilgileri',
    CC: 'Kart Numarası',
    dateEX: 'Son Kullanım',
    CH: 'Ad Soyad',
    buy: 'Satin Al',
    Refundable: 'İade Edilebilir',
    nonRefund: 'İade Edilemez',
    HB: 'Yarim Pansiyon',
    BB: 'Oda Kahvalti',
    AI: 'Herşey Dahil',
    RO: 'Sadece Oda',
    FB: 'Tam Pansiyon',
    FBP: 'Tam Kart Artı',
    UAI: 'Ultra Her Şey Dahil',
    tc: 'Aşağıdaki şartları ve koşulları kabul ediyorum',
    room: 'ODA',
    standard: "STANDART",
    deluxe: "DELUXE",
    premium: "ÖDÜL",
    de: 'Almanca',
    en: 'İngilizce',
    ru: 'Rusça',
    tr: 'Türkçe',
    ar: 'Arapça',
    ccNeededLater: 'Kredi kartı bilgileri garanti amaçlı alınır, ödenmez.',
    ccNeededNow: 'Ödeme şimdi alınacaktır.',
    ccNeededDownPayment: 'Ödeme 25% kapora ile şimdi alınacaktır.',
    resID: 'Rezervasyon Numarası',
    ConfID: 'Onay Numarası',
    roomtype: 'Oda tipi',
    hotelNumber: "Otel telefon numarası",
    boardType: 'Pansiyon tipi',
    specialreq: 'Özel istek',
    paymentType: "Ödeme şekli",
    cancelPoly: 'İptal politikası',
    adres: 'Adres',
    rateType: 'Fiyat tipi',
    saveReservation: 'Rezervasyonu kaydet',
    theme: 'Tema',
    paylater: 'Otelde öde',
    payCC: 'Kredi kartı ile öde',
    payWire: 'Banka havalesi ile öde',
    payLaterGarNeeded: 'Otelde öde (kredi kartı garantisi ile)',
    payDownPayment: 'Kapora ile öde',
    callME: 'Call Me',
    extra: 'Extra Information'
}


let germanLanguage = {
    w: 'Frau',
    m: 'Mann',
    bd: 'Geburtstag',
    name: 'Name',
    surname: 'Familienname',
    adult: 'Erwachsene',
    A: 'Erwachsene',
    C: 'Kind',
    sex: 'Geschlecht',
    Email: "Email",
    phone: 'Telefon',
    note: 'Hinweise für das Hotel',
    cl: 'Löschen',
    CI: 'Check-in Datum',
    CO: 'Überprüfe das Datum',
    srch: 'Suche',
    CAge: 'Kind Alter',
    N: "Nacht",
    SO: 'Ausverkauft',
    rooms: 'Zimmer',
    contactInfo: 'Kontakt Informationen',
    traveler: 'Gast Informationen',
    payInfo: 'Zahlungsinformationen',
    CC: 'Kreditkartennummer',
    dateEX: 'Ablaufdatum',
    CH: 'Name und Familienname',
    buy: 'Kaufen',
    Refundable: 'Erstattungsfähig',
    nonRefund: 'Nicht erstattungsfähig',
    HB: 'Halbpension',
    BB: 'Bed & Breakfast',
    AI: 'Alles inklusive',
    RO: 'Nur Zimmer',
    FB: 'Vollpension',
    FBP: 'Vollpension Plus',
    UAI: 'Ultra Alles inklusive',
    tc: 'Ich stimme den folgenden Bedingungen zu',
    room: 'ZIMMER',
    standard: "STANDARD",
    deluxe: "DELUXE",
    premium: "PRÄMIE",
    de: 'German',
    en: 'English',
    ru: 'Russian',
    tr: 'Turkish',
    ar: 'Arabic',
    ccNeededLater: 'Credit card information is taken for guarantee purposes it will NOT BE CHARGED',
    ccNeededNow: 'Credit card will be charged NOW',
    ccNeededDownPayment: 'Credit card will be charged now with 25% down payment',
    resID: 'Reservation ID',
    ConfID: 'Confirmation ID',
    roomtype: 'Room type',
    hotelNumber: "Hotel Phone Number",
    boardType: 'Board Type',
    specialreq: 'Special Request',
    paymentType: "Payment Type",
    cancelPoly: 'Cancellation Policy',
    adres: 'Address',
    rateType: 'Rate Type',
    saveReservation: 'Save Reservation',
    theme: 'Theme',
    paylater: 'Pay Later/At Hotel',
    payCC: 'Pay by Credit Card',
    payWire: 'Pay by Bank transfer',
    payLaterGarNeeded: 'Pay later garenti needed',
    payDownPayment: 'Pay by Down Payment',
    callME: 'Call Me',
    extra: 'Extra Information'
}


var russianLanguage = {
    w: 'Женщина',
    m:  'Мужчина',
    bd: 'Дата Рождения',
    name: 'Имя',
    surname: 'Фамилия',
    adult: 'Взрослый',
    A: 'Взрослый',
    C: 'Ребенок',
    sex: 'Пол',
    Email:  'Эл.почта',
    phone: 'Номер телефона',
    note: 'Заметки для отеля',
    cl:'Очистить',
    srch: 'Поиск',
    CI: 'Заселение',
    CO: 'Выселение',
    CAge: 'Возраст ребенка',
    N: 'Ночь',
    SO: 'Продано',
    rooms: 'Комнаты',
    contactInfo: 'Контактная информация',
    traveler: 'Путешественник',
    payInfo: 'Информация о платеже',
    CC: 'Номер кредитной карты',
    dateEX: 'Срок окончания дейтвия',
    CH: 'Имя Фамилия',
    buy: 'Купить',
    Refundable: 'возвращаемый',
    nonRefund: 'Не подлежащий возврату',
    HB: 'Полупансион',
    BB: 'кровать и завтрак',
    AI: 'Все включено',
    RO: 'Только комната',
    FB: 'Полный пансион',
    FBP: 'Полный пансион Plus',
    UAI: 'Ultra Все включено',
    tc: 'Я соглашаюсь на следующие положения и условия',
    room: 'Комнаты',
    standard: "стандарт",
    deluxe: "роскошный",
    premium: "премия",
    de: 'German',
    en: 'English',
    ru: 'Русский',
    tr: 'Turkish',
    ar: 'Arabic',
    ccNeededLater: 'Данные кредитной карты взяты с целью предоставления гарантии. Плата не взимается',
    ccNeededNow: 'Сумма оплаты будет снята с кредитной карты сейчас',
    ccNeededDownPayment: 'Credit card will be charged now with 25% down payment',
    resID: 'Код бронирования',
    ConfID: 'Код подтверждения',
    roomtype: 'Тип Номера',
    hotelNumber: "Номер телефона отеля",
    boardType: 'Тип питания',
    specialreq: 'Специальный Запрос ',
    paymentType: "Тип платежа",
    cancelPoly: 'Политика отмены',
    adres: 'Адрес',
    rateType: 'Тип тарифа',
    saveReservation: 'Сохранить бронирование',
    theme: 'Тема',
    paylater: 'Оплатить В отеле',
    payCC: 'Оплатить картой',
    payWire: 'Оплатить переводом',
    payLaterGarNeeded: 'pay later ',
    payDownPayment: 'Pay by Down Payment',
    callME: 'Call Me',
    extra: 'Extra Information'
}


var arabicLanguage = {
    w: 'أنثى',
    m: 'ذكر',
    bd: 'تاريخ الميلاد',
    name: 'اسم',
    surname: 'لقب',
    adult: 'بالغ',
    A: 'بالغ',
    C: 'طفل',
    sex: 'جنس',
    Email: "البريد الإلكتروني",
    phone: 'رقم هاتف',
    note: 'ملاحظات للفندق',
    cl: 'مسح',
    srch: 'بحث',
    CI: 'تسجيل الدخول',
    CO: 'تسجيل الخروج',
    CAge: 'سن الطفل',
    N: "ليلة",
    SO: 'بيعت كلها',
    rooms: 'غرف',
    contactInfo: 'معلومات الاتصال',
    traveler: 'مسافرين',
    payInfo: 'معلومات الدفع',
    CC: 'رقم بطاقة الائتمان',
    dateEX: 'تاريخ انتهاء',
    CH: 'اسم ولقب',
    buy: 'شراء',
    Refundable: 'ممكن السداد',
    nonRefund: 'غير قابلة للاسترداد',
    HB: 'نصف إقامة',
    BB: 'سرير و فطور',
    AI: 'الجميع مشمول',
    RO: 'غرفة فقط',
    FB: 'ممتلئ',
    FBP: 'Full Board Plus',
    UAI: 'Ultra All Inclusive',
    tc: 'أوافق على البنود والشروط التالية',
    standard: "اساسي",
    deluxe: "فاخر",
    premium: "استثنائي",
    room: 'غرفة',
    de: 'German',
    en: 'English',
    ru: 'Russian',
    tr: 'Turkish',
    ar: 'عربى',
    ccNeededLater: 'يتم أخذ معلومات من بطاقة الائتمان لأغراض الضمان ولن يستقطع اي مبلغ منه',
    ccNeededNow: 'سيتم فرض رسوم على بطاقة الائتمان الآن',
    ccNeededDownPayment: 'Credit card will be charged now with 25% down payment',
    resID: 'الهوية التعريفية للحجز',
    ConfID: 'الهوية التعريفية للتاكيد',
    roomtype: 'نوع الغرفة',
    hotelNumber: "رقم هاتف الفندق",
    boardType: 'نوع البورد',
    specialreq: 'طلب خاص',
    paymentType: "نوع الدفع",
    cancelPoly: 'سياسة الإلغاء',
    adres: 'عنوان',
    rateType: 'نوع التقييم',
    saveReservation: 'حفظ الحجز',
    theme: 'ثيم',
    paylater: 'دفع في وقت لاحق',
    payCC: 'الدفع بواسطة بطاقة الائتمان',
    payWire: 'الدفع عن طريق التحويل المصرفي',
    payLaterGarNeeded: 'دفع لاحقا ولكن هناك حاجة إلى ضمان',
    payDownPayment: 'Pay by Down Payment',
    callME: 'Call Me',
    extra: 'Extra Information'

}


var roomHeaderString
var lang

function setLanguageAndReturnDictonary(x = 'en', days, peopleSelected = null, rooms = null,roomIsChoosen, fillInRoomsResp=null) {
    lang = x

    if (x.toLowerCase() === "en"){
        picked = englishLanguage
        document.getElementById('languagePlaceHolder').innerHTML = picked.en +'<span class="caret"></span>'

    }
    else if(x.toLowerCase() === "ar"){
        picked = arabicLanguage
        document.getElementById('languagePlaceHolder').innerHTML = picked.ar +'<span class="caret"></span>'

    }
    else if(x.toLowerCase() === 'de'){
        picked = germanLanguage
        document.getElementById('languagePlaceHolder').innerHTML = picked.de +'<span class="caret"></span>'

    }
    else if (x.toLowerCase() === 'ru'){
        picked = russianLanguage
        document.getElementById('languagePlaceHolder').innerHTML = picked.ru +'<span class="caret"></span>'

    }
    else{
        picked = turkishLanguage
        document.getElementById('languagePlaceHolder').innerHTML = picked.tr +'<span class="caret"></span>'
    }


    changethemes()
    document.getElementById('extraInformation').value = picked.extra + ' <span class="caret"></span>'
    document.getElementById('openSaveRegistrationPage').value = picked.callME
    document.getElementById('saveResButtonInfloatingScreen').innerHTML = picked.callME
    document.getElementById('phoneNumberSaveLabel').innerHTML = picked.phone
    document.getElementById('idFindResLabel').innerHTML = picked.resID
    document.getElementById('nameOnCardLabel').innerHTML = picked.CH
    document.getElementById('checkInLabel').innerHTML = picked.CI
    document.getElementById('checkOutLabel').innerHTML = picked.CO
    document.getElementById('adultLabel').innerHTML = picked.adult
    document.getElementById('childLabel').innerHTML = picked.C
    document.getElementById('childAgeLabel').innerHTML = picked.CAge
    document.getElementById('searchButton').value = picked.srch
    document.getElementById('findResButton').innerHTML = picked.srch
    document.getElementById("roomPanelheader").innerHTML = picked.rooms
    document.getElementById('travelersPanelHeader').innerHTML = picked.traveler
    document.getElementById('contactInfoHeader').innerHTML = picked.contactInfo
    document.getElementById('paymentInfo').innerHTML = picked.payInfo
    document.getElementById('register').value = picked.buy

    document.getElementById("cardnumber").innerHTML = picked.CC + " *"
    document.getElementById('expireDate').innerHTML = picked.dateEX + " *"
    document.getElementById('sex').innerHTML = picked.sex
    
    document.getElementById('name').innerHTML = picked.name + " *"
    document.getElementById('surname').innerHTML = picked.surname + " *"
    document.getElementById("phoneLabel").innerHTML = picked.phone + " *"
    document.getElementById('emailLabel').innerHTML = picked.Email + " *"
    document.getElementById('noteLabel').innerHTML = picked.note
    document.getElementById('termsDiv').innerHTML = '<input id="terms" type="checkbox" class="checkboxCustom" value="false" onchange="checkButton()"><a id="termslink" class="checkboxCustom" tabindex="113">'+picked.tc+'</a>'
    document.getElementById('termslink').setAttribute('onclick','readText()')
    document.getElementById('surnameFindResLabel').innerHTML = picked.surname



    if (rooms !== null && roomIsChoosen === null){
        translateCCinfo()
        translateRooms(peopleSelected,rooms,days)
    }
    if (roomIsChoosen !== null && peopleSelected !== null && peopleSelected.adult !== 0){
        translateCCinfo()
        fillInPeople(peopleSelected,roomIsChoosen)
    }
    translateCCinfo()
    return picked
}

function translateBoardType(x){
    switch (x){
        case 'HB':
            return picked.HB
        case 'BB':
            return picked.BB
        case 'AI':
            return picked.AI
        case 'RO':
            return picked.RO
        case 'FB':
            return picked.FB
        case 'FBP':
            return picked.FBP
        case 'UAI':
            return picked.UAI
        default:
            return x
    }
}

function translateRateType(x) {


    if (x.includes('Refundable') || x.toLowerCase().includes('refundable') || x.toLowerCase().includes('rafundable')){

        if (x.includes('Not') || x.toLowerCase().includes('not') || x.includes('None') || x.toLowerCase().includes('none') || x.toLowerCase().includes('non')){
            return picked.nonRefund
        }
        else{
            return picked.Refundable
        }
    }
    else if (x.includes('İade') || x.includes('iade')) {

        if (x.includes('edilemez') || x.includes('Edilemez')){
            return  picked.nonRefund
        }
        else{
            return  picked.Refundable
        }
    }
    else{
        return x
    }
}

function fillInPeople(peopleSelected,i) {

    if (peopleSelected.child == ""){
        roomHeaderString = peopleSelected.adult + " " + picked.A + " " + differenceBetweenCheckInAndOut + " " + picked.N
    }
    else{
        roomHeaderString = peopleSelected.adult + " " + picked.A + " " + peopleSelected.child+ " "+ picked.C + " " + differenceBetweenCheckInAndOut + " " + picked.N
    }

    var roomtype = rooms[i].RoomType;
    var roomInfo = rooms[i].RoomInfo

    var dataPlacement = (screenWidth < 500)?'bottom': 'right';

    document.getElementById(''+roomtype+'').innerHTML =  translateRoomType(roomtype)+'<a data-placement="'+dataPlacement+'" title="'+roomInfo+'" data-toggle="tooltip" style="text-decoration: none;" ><img  src="images/3716.svg" width="13px" height="13px"></a>'

    if (i%2===0){document.getElementById('roomHeaderString'+i+'').innerHTML = roomHeaderString}
    document.getElementById('rateType'+i+'').innerHTML = translateRateType(rooms[i].RateType);
    document.getElementById('boardtype'+i+'').innerHTML = translateBoardType(rooms[i].BoardType);
    var sum = peopleSelected.adult + peopleSelected.child


    $(document).ready(function(){
        if (screenWidth < 500){
            $('[data-toggle="tooltip"]').tooltip({trigger: "click"})
        }
        else{
            $('[data-toggle="tooltip"]').tooltip({trigger: "hover"})
        }

    });


    for (var i = 1; i <= sum;i++){


        document.getElementById('sexClassFemale'+i+'').innerHTML = picked.w
        document.getElementById('sexClassMale'+i+'').innerHTML = picked.m

    }

}

function translateRoomType(room) {

    room = room.split(" ")

    switch (room[0].toLowerCase()){
        case 'deluxe':
            if (lang == 'ar'){
                return  picked.room + ' ' + picked.deluxe
            }
            return picked.deluxe + ' ' + picked.room
        case 'premium':
            if (lang == 'ar'){
                return  picked.room + ' ' + picked.premium
            }
            return picked.premium + ' ' + picked.room

        case 'superior':
                    if (lang == 'ar'){
                        return  picked.room + ' ' + picked.standard
                    }
            return picked.standard + ' ' + picked.room


        default:
            if (lang == 'ar'){
                return  picked.room + ' ' + room[0]
            }
            return room[0] + ' ' + picked.room
    }

}

function translateRooms(peopleSelected,rooms,days){

    if (peopleSelected.child == ""){
        roomHeaderString = peopleSelected.adult + " " + picked.A + " " + days + " " + picked.N
    }
    else{
        roomHeaderString = peopleSelected.adult + " " + picked.A + " " + peopleSelected.child+ " "+ picked.C + " " + days + " " + picked.N
    }


    for (var i=0; i<rooms.length;i++){

        var boardtype = translateBoardType(rooms[i].BoardType);
        var ratetype = translateRateType(rooms[i].RateType);
        var dataPlacement = (screenWidth < 500)?'bottom': 'right';
        var roomtype = rooms[i].RoomType;
        var roomInfo = rooms[i].RoomInfo

        var m = translateRoomType(roomtype)

        document.getElementById(''+roomtype+'').innerHTML =  translateRoomType(roomtype)+'<a data-placement="'+dataPlacement+'" title="'+roomInfo+'" data-toggle="tooltip" style="text-decoration: none;" ><img  src="images/3716.svg" width="13px" height="13px"></a>'

        if (i%2===0 && document.getElementById('roomHeaderString'+i+'') !== null){
            document.getElementById('roomHeaderString'+i+'').innerHTML = roomHeaderString
        }

        document.getElementById('rateType'+i+'').innerHTML = ratetype
        document.getElementById('boardtype'+i+'').innerHTML = boardtype
        if (rooms[i].IsAvailable !== true){
            document.getElementById('buyButton'+i+'').value = picked.SO
        }


        $(document).ready(function(){
            if (screenWidth < 500){
                $('[data-toggle="tooltip"]').tooltip({trigger: "click"})
            }
            else{
                $('[data-toggle="tooltip"]').tooltip({trigger: "hover"})
            }

        });
    }
}

function changethemes(){
    document.getElementById('themes').innerHTML = picked.theme + '<span class="caret"></span>'
    var roomClassDelete = document.getElementById('themesTable')
    while (roomClassDelete.hasChildNodes()) {
        roomClassDelete.removeChild(roomClassDelete.lastChild);
    }
    for (var i = 1; i <= 11; i++){
        document.getElementById('themesTable').innerHTML += '<li class="pointerCurser"><a id="s'+i+'" >'+picked.theme+ ' ' + i +'</a></li>'
    }
    $("#s1").click(function() {
        $("#changestyle").attr("href", "css/bootstrap.min1.css");
    })
    $("#s2").click(function() {
        $("#changestyle").attr("href", "css/bootstrap.min2.css");
    })
    $("#s3").click(function() {
        $("#changestyle").attr("href", "css/bootstrap.min3.css");
    })
    $("#s4").click(function() {
        $("#changestyle").attr("href", "css/bootstrap.min4.css");
    })
    $("#s5").click(function() {
        $("#changestyle").attr("href", "css/bootstrap.min5.css");
    })
    $("#s6").click(function() {
        $("#changestyle").attr("href", "css/bootstrap.min6.css");
    })
    $("#s7").click(function() {
        $("#changestyle").attr("href", "css/bootstrap.min7.css");
    })
    $("#s8").click(function() {
        $("#changestyle").attr("href", "css/bootstrap.min8.css");
    })
    $("#s9").click(function() {
        $("#changestyle").attr("href", "css/bootstrap.min9.css");
    })
    $("#s10").click(function() {
        $("#changestyle").attr("href", "css/bootstrap.min10.css");
    })
    $("#s11").click(function() {
        $("#changestyle").attr("href", "css/bootstrap.min11.css");
    })

}

function translateCCinfo(){

    if (radioValue === 4){
        document.getElementById('creditCardChargePolicy').innerHTML = picked.ccNeededLater
    }

    document.getElementById('paybyCCLabel').innerHTML = picked.payCC
    document.getElementById('payBywireLabel').innerHTML = picked.payWire
    document.getElementById('payLaterLabel').innerHTML = picked.paylater

    document.getElementById('payLaterWithGuaranteeLabel').innerHTML = picked.payLaterGarNeeded
    document.getElementById('payByDownPaymentLabel').innerHTML = picked.payDownPayment
}

