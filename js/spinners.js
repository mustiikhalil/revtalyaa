function subAdultSpinner(){
    var value = parseInt(document.getElementById('adult').value)

    if (rooms !== null){checkIfRoomsAlreadyShown()}
    if (value > parseInt(document.getElementById('adult').min)){
        document.getElementById('adult').value = value - 1

    }


}

function addAdultSpinner(){
    var value = parseInt(document.getElementById('adult').value)


    if (rooms !== null){checkIfRoomsAlreadyShown()}
    if (value < parseInt(document.getElementById('adult').max)){
        document.getElementById('adult').value = value + 1

    }
}

function subChildSpinner(){
    var value = parseInt(document.getElementById('child').value)

    if (rooms !== null){checkIfRoomsAlreadyShown()}
    if (value > parseInt(document.getElementById('child').min)){
        document.getElementById('child').value = value - 1

    }

}

function addChildSpinner(){
    var value = parseInt(document.getElementById('child').value)

    if (rooms !== null){checkIfRoomsAlreadyShown()}
    if (value < parseInt(document.getElementById('child').max)){
        document.getElementById('child').value = value + 1

    }
}

function checkIfRoomsAlreadyShown() {


        var roomClassDelete = document.getElementById('roomsClassDelete')

        while (roomClassDelete.hasChildNodes()) {
            roomClassDelete.removeChild(roomClassDelete.lastChild);
        }

        var renderAfterPick = document.getElementById('renderAfterPick')

        while (renderAfterPick.hasChildNodes()) {
            renderAfterPick.removeChild(renderAfterPick.lastChild);
        }
        document.getElementById('searchButton').setAttribute('onclick', 'searchAgain()')
        $('.roomPanel').css('display','none')
        $(".guestPanel").css("display", "none");
        $('#roomHeader').removeClass('active');
        $('#paymentHeader').removeClass('active');
        $('#searchHeader').addClass('active')

}

