var extras = []
function saveReservation(){

    if (radioValue === 5){
        radioValue = 3
    }
    placeExtraServices()
    var arrayOfDates = GetDates()
    pickeddatePickerCheckInDate = arrayOfDates[0]
    pickeddatePickerCheckOutDate = arrayOfDates[1]

    postSend.HotelID = (hotelId === null) ? '': parseInt(hotelId);
    postSend.Arrival = (pickeddatePickerCheckInDate === null) ? '' : pickeddatePickerCheckInDate;
    postSend.Departure = (pickeddatePickerCheckOutDate === null) ? '' : pickeddatePickerCheckOutDate;
    postSend.Adult = (peopleSelected.adult === null) ? '' : peopleSelected.adult;
    postSend.Currency = (roomSelected.currency === undefined) ? 'TRY' : roomSelected.currency
    postSend.RoomTypeId = (roomSelected.roomTypeID === undefined) ? 0 : roomSelected.roomTypeID;
    postSend.RateTypeId = (roomSelected.ratetypeId === undefined) ? 0 : roomSelected.ratetypeId;
    postSend.BoardTypeId = (roomSelected.boardtypeId === undefined) ? 0 : roomSelected.boardtypeId
    postSend.Price = (roomSelected.price === undefined) ? 0 : roomSelected.price
    postSend.CardCvv = (document.getElementById('CVC').value === null) ? '' : document.getElementById('CVC').value;
    postSend.CardExpires = (document.getElementById('expmonth').value === null) ? '' : document.getElementById('expmonth').value +'/' +document.getElementById('expyear').value;

    postSend.CardNo = document.getElementById('pan1').value + '' + document.getElementById('pan2').value + "" + document.getElementById('pan3').value +""+document.getElementById('pan4').value;

    postSend.PaymentType = (radioValue === null) ? "" : radioValue

    if (peopleSelected.child !== null && peopleSelected.child !== "") {
        var numPeople = parseInt(peopleSelected.adult) + parseInt(peopleSelected.child)
    }
    else{
        var numPeople = parseInt(peopleSelected.adult)
    }

    if (numPeople === 0){
        postSend.People.push(new person('E',null,'noName',""));
    }
    else{
        for (var i = 1; i <= numPeople; i++) {
            var peopleToBeAdded = new person(document.getElementById('sex' + i + '').value, null , document.getElementById('name' + i + '').value, document.getElementById('surname' + i + '').value)
            postSend.People.push(peopleToBeAdded);

        }
    }

    var childAges = ""
    for(var i = 0; i < peopleSelected.child; i++){
        childAges += document.getElementById('chd_AGE'+i+'').value + " "
    }

    postSend.ChdAges = childAges
    postSend.Email = (document.getElementById('email').value === "") ? "noemail" : document.getElementById('email').value


    if (document.getElementById('telephone').value === ""){
        postSend.Phone = document.getElementById('phoneNumberSave').value

    }
    else{
        postSend.Phone = document.getElementById('telephone').value
    }


    postSend.CardHolder = document.getElementById('nameOnCard').value;

    postSend.PortalReferenceId = document.getElementById('refID').value;
    if (postSend.Phone !== "" && postSend.Phone !== null){
        saveResAJAXCall(postSend)
        for (i = 1; i <= numPeople; i++) {
            postSend.People.pop()
        }

    }
}

function closeFloatingShowReservationPage(){
    document.getElementById('fullHTML').classList.remove('blur')
    document.getElementById('savedReservationPanel').setAttribute('style','display: none !important')
    document.getElementById('reservationSavedID').value = ""
}

function searchForSavedReservation(){
    document.getElementById('searchButton').removeAttribute('onclick')
    var savedResID = document.getElementById('reservationSavedID').value
    showSavedReservation(savedResID)
}

var savedResRooms = null;

function waitingForHotelParametersToBeSet(getReservationThatWasSaved){
    removeIfSearchedAgain()
    var parsedJsonSavedRes = JSON.parse(getReservationThatWasSaved.ReservationRequest)
    hotelId = parsedJsonSavedRes.HotelID
    getHotelconfigAJAXIDForSavedRes(parsedJsonSavedRes.HotelID,parsedJsonSavedRes)

}

function setParam(parsedJsonSavedRes) {
    changeLan(selectedLang)
    var numberOfChildren = (null === parsedJsonSavedRes.childAges) ? "" : "1"

    var thisURL = "https://api.hoteladvisor.net/Hotel/GetHotelRooms?id=" + parsedJsonSavedRes.HotelID + "&Arrival=" + parsedJsonSavedRes.Arrival + "&Departure=" + parsedJsonSavedRes.Departure + "&Adult=" + parsedJsonSavedRes.Adult + "&ChdAges=" + numberOfChildren + "&Currency=TRY";

    savedPeopleToShow(parsedJsonSavedRes.People)
    findSavedRoom(thisURL, parsedJsonSavedRes)

    if(parsedJsonSavedRes.paymentType !== 2 || parsedJsonSavedRes.paymentType !== 3){

    }

    document.getElementById('CVC').value = (parsedJsonSavedRes.CardCvv !== null) ? parsedJsonSavedRes.CardCvv : '';
    document.getElementById('email').value = (parsedJsonSavedRes.Email !== 'noemail') ? parsedJsonSavedRes.Email : '';
    document.getElementById('telephone').value = (parsedJsonSavedRes.Phone !== null) ? parseInt(parsedJsonSavedRes.Phone) : "";
    document.getElementById('nameOnCard').value = (parsedJsonSavedRes.CardHolder !== null) ? parsedJsonSavedRes.CardHolder : "";
    document.getElementById('adult').value = (parsedJsonSavedRes.Adult !== null) ? parsedJsonSavedRes.Adult : "";
    document.getElementById('refID').value = (parsedJsonSavedRes.PortalReferenceId !== null) ? parsedJsonSavedRes.PortalReferenceId : "" ;
    if (parsedJsonSavedRes.paymentType === 1){//cc  1-cc 2-wire 3-payatotel 4-payatotelwithcc
        document.getElementById('payByCreditRadio').checked = true;
        radioValue = 1;

    }
    else if (parsedJsonSavedRes.paymentType === 2){//with wire

        document.getElementById('payByWireRadio').checked = true;
        radioValue = 2;

    }
    else if (parsedJsonSavedRes.paymentType == 5){//at hotel with guarentee
        document.getElementById('payLaterWithGuaranteeRadio').checked = true;
        radioValue = 5;

    }
    else if(parsedJsonSavedRes.paymentType == 4){//py by down payment
        document.getElementById('payByDownPaymentRadio').checked = true;
        radioValue = 4;

    }
    else{ //pay at hotel
        document.getElementById('wireTransferHolder').style = 'display: none'
        document.getElementById('CreditCardDiv').removeAttribute('style')
        document.getElementById('CreditCardDiv').setAttribute('style','display: none')
        document.getElementById('creditCardChargePolicy').setAttribute('style','display: visible')
        document.getElementById('payLaterRadio').checked = true;
        radioValue = 3;

    }


    peopleSelected.adult = parsedJsonSavedRes.Adult
    showChildrenOnRes(parsedJsonSavedRes)
    fillInCreditCard(parsedJsonSavedRes.CardNo, parsedJsonSavedRes.CardExpires)

    var checkIn = (parsedJsonSavedRes.Arrival !== null) ? new moment(parsedJsonSavedRes.Arrival) : new moment()
    var checkOut = (parsedJsonSavedRes.Departure !== null) ? new moment(parsedJsonSavedRes.Departure) : new moment()


    setAttrForSearchAgain()
    setDateAndDisableInputFields(checkIn,checkOut)
    fixCSS()
}

function showRoomThatWasSaved(savedResRooms,reservation) {

    for (var i = 0; i < savedResRooms.length; i++){
        if (savedResRooms[i].RateTypeId === reservation.RateTypeId && savedResRooms[i].BoardTypeId === reservation.BoardTypeId && savedResRooms[i].RoomTypeId === reservation.RoomTypeId){
            rooms = savedResRooms
            keepChosenRoom(i)
        }
    }
}

function fixCSS(){
    $(".roomPanel").css("display","block");
    $(".guestPanel").css("display", "block");
    $('#searchHeader').removeClass('active');
    $('html,body').animate({scrollTop: $(".guestPanel").offset().top - 120}, 500);
    $('#paymentHeader').addClass('active');
    $('#savedReservationPanel').css('display','none')
    $('#fullHTML').removeClass('blur')
}

function savedPeopleToShow(peopleArray) {
    var tabIndex = 4
    var container = document.getElementById("peoplesTable");
    container.innerHTML = '<tr><th id="sex" class="col-md-2 col-xs-3 textLabels" >'+languageDictonary.sex+'</th> <th id="name"  class="textLabels col-md-4 col-xs-4">'+languageDictonary.name+' *</th> <th id="surname" class="textLabels col-md-4 col-xs-5" >'+languageDictonary.surname+' *</th> </tr>'


    for (var i = 1; i <= peopleArray.length; i++){

        var sex = (i%2 === 0)? '<select id="sex'+ i +'"size="1" name="cinsiyet" class="form-control" tabindex="' + tabIndex + '" ><option id="sexClassFemale'+i+'" value="K" selected="selected">'+languageDictonary.w+'</option> <option id="sexClassMale'+i+'" value="M">'+languageDictonary.m+'</option> </select>' :'<select id="sex' + i + '"size="1" name="cinsiyet" class="form-control" tabindex="' + tabIndex + '" ><option id="sexClassFemale'+i+'" value="K" >'+languageDictonary.w+'</option> <option id="sexClassMale'+i+'" value="E" selected="selected">'+languageDictonary.m+'</option> </select>'

        container.innerHTML += '<tr> <td class="col-md-2 col-xs-3">'+sex+'</td><td class="col-md-4 col-xs-4"><input id="name' + i + '"type="text" size="15" name="ad" value="" class="form-control" maxlength="20" tabindex="' + tabIndex + '" ></td>'+
            '<td class="col-md-4 col-xs-5"><input id="surname' + i + '" type="text" size="15" name="soyad" value="" class="form-control" maxlength="20" tabindex="' + tabIndex + '" ></td></tr>'

        tabIndex++
    }

    for (var i = 1; i <= peopleArray.length; i++){

        document.getElementById('sex'+i+'').value = peopleArray[i-1].Gender
        document.getElementById('name'+i+'').value = peopleArray[i-1].FirstName
        document.getElementById('surname'+i+'').value = peopleArray[i-1].LastName
    }
}

function fillInCreditCard(ccNumber,exDate){

    var expDateArray = (exDate !== null) ? exDate.split('/') : ['01','2017'];
    document.getElementById('expmonth').value = expDateArray[0]
    document.getElementById('expyear').value = expDateArray[1]

    var ccNumberStr = (ccNumber !== null) ? ccNumber.match(/.{1,4}/g): ""
    if (ccNumberStr !== ""){
        for (var i = 0; i< ccNumberStr.length; i++){
            document.getElementById('pan'+(i+1)+'').value = ccNumberStr[i]
        }
    }
}

function showChildrenOnRes(parsedJsonSavedRes) {
    if (parsedJsonSavedRes.ChdAges) {

        var arrayChild = parsedJsonSavedRes.ChdAges.split(" ")
        peopleSelected.child = arrayChild.length - 1
        document.getElementById('child').value = arrayChild.length - 1

        var csi = $("#child").val();
        if (csi > 0) {
            $(".clbl").show();
        }
        for (var i = 0; i < arrayChild.length - 1; i++) {

            $('#chd_AGE'+i+'').show();
            $('#chd_AGE' + i + '').val(arrayChild[i])
        }
        for (var i = 5; i >= arrayChild.length - 1; i--){
            $('#chd_AGE'+i+'').hide();
        }

    }
}

function removeIfSearchedAgain(){

    var roomClassDelete = document.getElementById('renderAfterPick')
    while (roomClassDelete.hasChildNodes()) {
        roomClassDelete.removeChild(roomClassDelete.lastChild);
    }
}

function setDateAndDisableInputFields(In,Out){
    $('#datetimepicker1').data('DateTimePicker').date(In)
    $('#datetimepicker2').data('DateTimePicker').minDate(Out)
    $('#datetimepicker2').data('DateTimePicker').date(Out)
}

function setParametersForRese(respones) {

    searchConfig = respones;
    showCurrencries()
    currencyTheUserPicked = "TRY"
    if (searchConfig.LogoURL !== null && searchConfig.LogoURL !== ""){
        document.getElementById('hotelLink').innerHTML =  '<img src="'+ searchConfig.LogoURL+'" alt="" width="60px" height="60px"/>' + '<p style="float: left;margin-left: 67px;margin-top: -38px;"class="navbar-text">'+searchConfig.HotelName+'</p>'

    }
    else{
        var pHotelName = document.createElement('p')
        pHotelName.setAttribute('style','float: left;margin-left: 15px;margin-top: 22px;')
        pHotelName.setAttribute('class','navbar-text')
        pHotelName.innerHTML = searchConfig.HotelName
        document.getElementById('hotelLink').appendChild(pHotelName)
    }



    document.getElementById('hotelLink').href = "http://" + searchConfig.WebURL;
    if (searchConfig.CreditCardRequired === false && searchConfig.PayAtHotel === true){
        document.getElementById('paymentForm').setAttribute('style','display: none')
    }
    else if (searchConfig.CreditCardRequired === true && searchConfig.PayAtHotel === true){
        document.getElementById('creditCardChargePolicy').innerHTML = languageDictonary.ccNeededLater
    }
    else{
        document.getElementById('creditCardChargePolicy').innerHTML = languageDictonary.ccNeededNow
    }


    setTime();
    setCountOfPeople();
    setAttr();
    document.getElementById('searchButton').disabled = false;
}

function saveReservationFloatingPage(){
    if (document.getElementById('telephone').value === ""){
        document.getElementById('fullHTML').setAttribute('class','blur')
        document.getElementById('saveResFloating').removeAttribute('style')
        document.getElementById('saveResFloating').setAttribute('style','display: block')
    }
    else{
        saveReservation()
    }

}

function closeFloatingSavePage() {
    document.getElementById('findMyReservation').removeAttribute('style')
    document.getElementById('fullHTML').classList.remove('blur')
    document.getElementById('saveResFloating').setAttribute('style','display: none !important')
    document.getElementById('saveResFloating').setAttribute('style','display: none')
    document.getElementById('phoneNumberSave').value = ""

}


function callMeButtonClicked(){

        document.getElementById('findMyReservation').removeAttribute('style')
        document.getElementById('fullHTML').classList.remove('blur')
        document.getElementById('saveResFloating').setAttribute('style','display: none !important')
        document.getElementById('saveResFloating').setAttribute('style','display: none')
        saveReservation()
        document.getElementById('phoneNumberSave').value = ""


}