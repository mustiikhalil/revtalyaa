
var currencyTheUserPicked

function fillInRooms(respones) {
    rooms = respones
    
    var dataPlacement = 'right';
    if (screenWidth < 500) {
        dataPlacement = 'bottom'
    }

    var container = document.getElementById("roomsClassDelete");


    if (rooms.length){
        for (var i = 0; i < rooms.length; i++) {

            var roomInfo = (rooms[i].RoomInfo === null) ? "" : rooms[i].RoomInfo;
            var boardtype = (rooms[i].BoardType === null) ? "" : rooms[i].BoardType;
            var boardtypeID = (rooms[i].BoardTypeId === null) ? "" : rooms[i].BoardTypeId;
            var currency = (rooms[i].Currency === null) ? "" : rooms[i].Currency;
            var price = (rooms[i].Price === null) ? "" : rooms[i].Price;
            var ratetype = (rooms[i].RateType === null) ? "" : rooms[i].RateType;
            var roomImageURL = (rooms[i].RateType === null || rooms[i].RoomImageURL === undefined) ? "" : rooms[i].RoomImageURL;
            var roomtype = (rooms[i].RoomType === null) ? "" : rooms[i].RoomType;
            var roomTypeId = (rooms[i].RoomTypeId === null) ? "" : rooms[i].RoomTypeId;
            var isAvailable = rooms[i].IsAvailable

            if (document.getElementById(roomtype) === null) {

                var outerBoarder = document.createElement("div");
                outerBoarder.setAttribute('class', 'col-md-12 col-xs-12 holderBoarder')
                // outerBoarder.setAttribute('id','OuterBoarder'+roomtype+'')

                var mainRoomHolder = document.createElement('div');
                mainRoomHolder.setAttribute('class', 'col-md-12 col-xs-12 mainRoomHolder')
                outerBoarder.appendChild(mainRoomHolder)

                var placeHolder = document.createElement('div');
                placeHolder.setAttribute('class', 'col-md-12 col-xs-12 placeHolders')
                placeHolder.setAttribute('id', 'placeholder' + roomtype + '')
                outerBoarder.appendChild(placeHolder)

                var imgSpan = document.createElement('span');
                imgSpan.setAttribute('class', 'roomPicture')
                mainRoomHolder.appendChild(imgSpan)

                if (roomImageURL !== "") {
                    var imgInSpan = document.createElement('img');
                    imgInSpan.setAttribute('src', roomImageURL)
                    imgInSpan.setAttribute('class', ' img-responsive')
                    imgInSpan.setAttribute('width', ' 100')
                    imgInSpan.setAttribute('height', ' 90')
                    imgInSpan.setAttribute('alt', 'hotelRoom')
                    imgSpan.appendChild(imgInSpan)
                }

                var spanForRoomType = document.createElement('span')
                mainRoomHolder.appendChild(spanForRoomType)

                var roomTypeNameOnBoarder = document.createElement('p')
                roomTypeNameOnBoarder.setAttribute('id', roomtype)
                roomTypeNameOnBoarder.setAttribute('class', 'roomTypeCSS roomTypeName')
                roomTypeNameOnBoarder.innerHTML = mainRoomType(roomtype)
                spanForRoomType.appendChild(roomTypeNameOnBoarder)

                var linkInfo = document.createElement('a')
                linkInfo.setAttribute('data-placement', dataPlacement)
                linkInfo.setAttribute('title', roomInfo)
                linkInfo.setAttribute('data-toggle', 'tooltip')
                linkInfo.style.textDecoration = 'none'
                roomTypeNameOnBoarder.appendChild(linkInfo)

                var infoImg = document.createElement('img')
                infoImg.setAttribute('src', 'images/3716.svg')
                infoImg.setAttribute('width', '13px')
                infoImg.setAttribute('height', '13px')
                linkInfo.appendChild(infoImg)

                var headerString = document.createElement('span')
                mainRoomHolder.appendChild(headerString)

                var headerStringInfo = document.createElement('p')
                headerStringInfo.setAttribute('id', 'roomHeaderString' + i + '')
                headerStringInfo.setAttribute('class', 'nightsPeopleCSS')
                headerStringInfo.innerHTML = funcHeaderString()
                headerString.appendChild(headerStringInfo)

                container.appendChild(outerBoarder)

                $(document).ready(function () {
                    if (screenWidth < 500) {
                        $('[data-toggle="tooltip"]').tooltip({trigger: "click"})
                    }
                    else {
                        $('[data-toggle="tooltip"]').tooltip({trigger: "hover"})
                    }

                });

            }

            if (document.getElementById(roomtype) !== null) {


                var roomRow = document.createElement('div')
                roomRow.setAttribute('class', 'rowRoom')
                document.getElementById('placeholder' + roomtype + '').appendChild(roomRow)

                var rateTypeInRow = document.createElement('div')
                rateTypeInRow.setAttribute('id', 'rateType' + i + '')
                rateTypeInRow.setAttribute('class', 'rateType col-lg-3 col-xs-4')
                rateTypeInRow.innerHTML = translateRateType(ratetype);
                roomRow.appendChild(rateTypeInRow)


                var boardTypeInRow = document.createElement('div')
                boardTypeInRow.setAttribute('id', 'boardtype' + i + '')
                boardTypeInRow.setAttribute('class', 'boardtype col-lg-3 col-xs-5')
                boardTypeInRow.innerHTML = translateBoardType(boardtype);
                roomRow.appendChild(boardTypeInRow)

                var buttonDIV = document.createElement('div')
                buttonDIV.setAttribute('class', 'col-lg-5 p0 col-xs-3 pullRight')
                roomRow.appendChild(buttonDIV)

                var buttonInRow = document.createElement('input')

                buttonInRow.setAttribute('id', 'buyButton' + i + '')
                buttonInRow.setAttribute('type', 'button')

                if (isAvailable === false) {
                    buttonInRow.removeAttribute('class')
                    buttonInRow.setAttribute('class', ': btn btn-danger diselectBtn roomSelectButton')
                    buttonInRow.setAttribute('disabled', true)
                }
                else {
                    buttonInRow.setAttribute('class', 'btn btn-success selectBtn roomSelectButton')
                }
                var valueOfButton = (isAvailable === true) ? Math.round(price) + ' ' + currency + ' >' : languageDictonary.SO

                buttonInRow.setAttribute('value', valueOfButton)
                buttonInRow.setAttribute('onclick', 'disable();keepChosenRoom(' + i + ')')

                buttonDIV.appendChild(buttonInRow)
            }
            $(".selectBtn").click(function () {
                $('html,body').animate({scrollTop: $(".guestPanelUsers").offset().top - 400} + 200);
                $(".guestPanel").css("display", "block");
                $('#roomHeader').removeClass('active');
                $('#paymentHeader').addClass('active');
            });

        }
        renderCurrencies(currencyTheUserPicked, rooms)
    }
    else {

        var outerBoarder = document.createElement("div");
        outerBoarder.setAttribute('class', 'col-md-12 col-xs-12 holderBoarder')


        var mainRoomHolder = document.createElement('div');
        mainRoomHolder.setAttribute('class', 'col-md-12 col-xs-12')
        outerBoarder.appendChild(mainRoomHolder)


        var errorMessage = document.createElement('p')
        errorMessage.setAttribute('class','errorMessageRoomsNotAva')
        errorMessage.innerHTML = 'No rooms are available'
        mainRoomHolder.appendChild(errorMessage)
        container.appendChild(outerBoarder)
    }

}

var currencies = new HashTable()

function showCurrencries(){
    $.ajax({
        type: "GET",
        url: 'https://api.hoteladvisor.net/Hotel/CurrencyRate?hotelid=' + hotelId,
        success: function (respones) {

            for (var i = 0; i < respones.length; i++){

                if (currencies.search(respones[i].ToCurrency) !== null){
                    var toCurrencies = currencies.search(respones[i].ToCurrency)
                    toCurrencies.add(respones[i].FromCurrency,respones[i].Rate)
                    currencies.add(respones[i].ToCurrency,toCurrencies)
                }
                else{
                    var toCurrencies = new HashTable()
                    toCurrencies.add(respones[i].FromCurrency,respones[i].Rate)
                    currencies.add(respones[i].ToCurrency,toCurrencies)

                    var li = document.createElement('li')
                    var span = document.createElement('span')
                    li.setAttribute('class','pointerCurser')
                    li.setAttribute('onclick','changeCurrency("'+respones[i].ToCurrency+'")')
                    li.appendChild(span)
                    var a = document.createElement('a')
                    a.setAttribute('id','currency'+respones[i].ToCurrency+'')
                    a.setAttribute('class','langaugeCSS')
                    a.setAttribute('style','text-decoration:none; color: #6e6e6e')
                    a.innerHTML = respones[i].ToCurrency
                    span.appendChild(a)

                    var img = document.createElement('img')
                    img.setAttribute('src','images/'+respones[i].ToCurrency+'.png')
                    img.setAttribute('class', 'currencyIMGs')
                    span.appendChild(img)
                    document.getElementById('currencyDropDown').appendChild(li)

                }
            }

        }
    });
}


function changePickedRoomcurrency(C="TRY"){
    var numberOfRooms = currencies.search(rooms[roomIsChoosen].Currency)

    if (numberOfRooms.search(C) !== null){
        var valueOfButton = (rooms[roomIsChoosen].IsAvailable === true) ? Math.round((rooms[roomIsChoosen].Price/numberOfRooms.search(C))) + ' ' + C + ' >' : languageDictonary.SO
        document.getElementById('pickedRoomPrice').setAttribute('value',valueOfButton)
    }
    else{
        var valueOfButton = (rooms[roomIsChoosen].IsAvailable === true) ? Math.round(rooms[roomIsChoosen].Price) + ' ' + C + ' >' : languageDictonary.SO
        document.getElementById('pickedRoomPrice').setAttribute('value',valueOfButton)
    }

}

function renderCurrencies(C="TRY",rooms) {


    for (var i =0; i < rooms.length; i++){

        var numberOfRooms = currencies.search(rooms[i].Currency)

        if (numberOfRooms.search(C) !== null){

            var valueOfButton = (rooms[i].IsAvailable === true) ? Math.round((rooms[i].Price/numberOfRooms.search(C))) + ' ' + C + ' >' : languageDictonary.SO

            document.getElementById('buyButton'+i+'').setAttribute('value',valueOfButton)
        }
        else{
            var valueOfButton = (rooms[i].IsAvailable === true) ? Math.round(rooms[i].Price) + ' ' + C + ' >' : languageDictonary.SO

            document.getElementById('buyButton'+i+'').setAttribute('value',valueOfButton)
        }

    }
}

var roomSelected = {}

// keeps the picked room
function keepChosenRoom(i) {
    roomIsChoosen = i

    var dataPlacement = 'right';
    if (screenWidth < 500){
        dataPlacement = 'bottom'
    }

    var roomSel = {
        cancelPolicy: rooms[i].CancelPolicy ,
        boardtype: rooms[i].BoardType,
        boardtypeId: rooms[i].BoardTypeId,
        currency : rooms[i].Currency,
        price : rooms[i].Price,
        ratetype : translateRateType(rooms[i].RateType),
        ratetypeId: rooms[i].RateTypeId,
        roomImageURL : rooms[i].RoomImageURL,
        roomInfo : (rooms[i].RoomInfo === null)? "" : rooms[i].RoomInfo,
        roomtype : rooms[i].RoomType,
        roomTypeID : rooms[i].RoomTypeId,
    }

    var roomImageURL = (rooms[i].RateType === null || rooms[i].RoomImageURL === undefined) ? "" : rooms[i].RoomImageURL;
    roomSelected = roomSel

    var container = document.getElementById('renderAfterPick');
    var imgInSpan = document.createElement('img');
    if (roomImageURL !== "") {
        imgInSpan.setAttribute('src', roomImageURL)
        imgInSpan.setAttribute('class', ' img-responsive')
        imgInSpan.setAttribute('width', ' 100')
        imgInSpan.setAttribute('height', ' 90')
        imgInSpan.setAttribute('alt', 'hotelRoom')

    }

    container.innerHTML += 	'<div  value="' + roomSel.roomTypeID + '" class="col-md-12 col-xs-12 holderBoarder"><div class="col-md-12 col-xs-12 mainRoomHolder"><span id="pickedROOMImg" class="roomPicture">'+
        '</span> <span> <p id="'+roomSel.roomtype+'" class="roomTypeName">'+mainRoomType(roomSel.roomtype)+'<a id="selector" data-placement="'+dataPlacement+'" title="'+roomSel.roomInfo+'" data-toggle="tooltip" title="'+roomSel.roomInfo+'" style="text-decoration: none;"><img id="popOutID" src="images/3716.svg" width="13px" height="13px"></a></p> </span> <span> <p id="roomHeaderString'+i+'" class="nightsPeopleCSS">'+funcHeaderString()+'</p>'+
        '</span> </div> <div class="col-md-12 col-xs-12 placeHolders"> <div class="rowRoom "> <div id="rateType'+i+'"class="boardType col-lg-3 col-xs-4">'+roomSel.ratetype+'</div> <div id="boardtype'+i+'" class="rateType col-lg-3 col-xs-5">'+translateBoardType(roomSel.boardtype)+'</div> <div class="col-lg-5 p0 col-xs-3 pullRight">'+
        '<input id="pickedRoomPrice"  type="button" class="btn btn-success selectBtn roomSelectButton" value="'+Math.round(roomSel.price)+" "+ roomSel.currency+'" onclick="disable();keepChosenRoom('+i+')" disabled> </div></div>'

    if (roomImageURL !== "" ){
        document.getElementById('pickedROOMImg').appendChild(imgInSpan)
    }
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip({trigger: "click"});
    });
    changeCurrency(currencyTheUserPicked)
}


function funcHeaderString(){
    differenceBetweenCheckInAndOut = getDifferenceCheckIn_Out()
    if (peopleSelected.child == "") {
         return peopleSelected.adult + " " + languageDictonary.A + " " + differenceBetweenCheckInAndOut + " " + languageDictonary.N
    }
    else {
         return peopleSelected.adult + " " + languageDictonary.A + " " + peopleSelected.child + " " + languageDictonary.C + " " + differenceBetweenCheckInAndOut + " " + languageDictonary.N
    }
}


function mainRoomType(x) {

    room = x.split(" ")
    switch (room[0].toLowerCase()){
        case 'deluxe':
            if (lang == 'ar'){
                return  picked.room + ' ' + picked.deluxe
            }
            return picked.deluxe + ' ' + picked.room
        case 'premium':
            if (lang == 'ar'){
                return  picked.room + ' ' + picked.premium
            }
            return picked.premium + ' ' + picked.room

        case 'superior':
            if (lang == 'ar'){
                return  picked.room + ' ' + picked.standard
            }
            return picked.standard + ' ' + picked.room


        default:
            if (lang == 'ar'){
                return  picked.room + ' ' + room[0]
            }
            return room[0] + ' ' + picked.room
    }

}


/**
 * after picking a room all the rooms that are rendered on the screen will be removed
 *
 * id=roomsClassDelete this holds the rooms that are sent from the server to the
 * id=renderAfterPick would hold the only room that will stay which is rerendered by keepChosenRoom()
 */
function disable(){

    $(".roomSelectButton").attr("disabled","disabled");
    var roomClassDelete = document.getElementById('roomsClassDelete')

    while (roomClassDelete.hasChildNodes()) {
        roomClassDelete.removeChild(roomClassDelete.lastChild);
    }
    amountOfPeopleToShowOnScreen()

}